#include "bmp.h"

#include "io.h"

#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_COLORS 256

typedef struct color {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Color;

typedef struct bmp {
    uint32_t height;
    uint32_t width;
    Color palette[MAX_COLORS];
    uint8_t **a;
} BMP;

uint32_t round_up(uint32_t x, uint32_t n) {
    while (x % n != 0) {
        x++;
    }
    return x;
}

BMP *bmp_create(FILE *fin) {
    BMP *pbmp = calloc(1, sizeof(BMP));
    if (pbmp == NULL) {
        fprintf(stderr, "Could not allocate memory to BMP\n");
        exit(1);
    }

    uint8_t type1, type2;
    read_uint8(fin, &type1);
    read_uint8(fin, &type2);

    uint32_t skipped[8];
    read_uint32(fin, &skipped[0]);
    read_uint32(fin, &skipped[1]); //combined the 2 16s
    read_uint32(fin, &skipped[2]);

    uint32_t bitmap_header_size, width, height;
    read_uint32(fin, &bitmap_header_size);
    read_uint32(fin, &width);
    read_uint32(fin, &height);

    pbmp->width = width;
    pbmp->height = height;

    uint16_t uint16skip;

    read_uint16(fin, &uint16skip);

    uint16_t bits_per_pixel;
    uint32_t compression;
    read_uint16(fin, &bits_per_pixel);
    read_uint32(fin, &compression);

    read_uint32(fin, &skipped[3]);
    read_uint32(fin, &skipped[4]);
    read_uint32(fin, &skipped[5]);

    uint32_t colors_used;
    read_uint32(fin, &colors_used);

    read_uint32(fin, &skipped[6]);

    if (type1 != 'B') {
        fprintf(stderr, "Type 1 isn't B\n");
        exit(1);
    }

    if (type2 != 'M') {
        fprintf(stderr, "Type 2 isn't M\n");
        exit(1);
    }

    if (bitmap_header_size != 40) {
        fprintf(stderr, "Header size isn't 40\n");
        exit(1);
    }

    if (bits_per_pixel != 8) {
        fprintf(stderr, "bits per pixel isn't 8\n");
        exit(1);
    }

    if (compression != 0) {
        fprintf(stderr, "compression isn't 0\n");
        exit(1);
    }

    uint32_t num_colors = colors_used;
    if (num_colors == 0)
        num_colors = (1 << bits_per_pixel);

    uint8_t red, green, blue, skipped_colors[num_colors];
    for (uint32_t i = 0; i < num_colors; i++) {
        read_uint8(fin, &blue);
        pbmp->palette[i].blue = blue;

        read_uint8(fin, &green);
        pbmp->palette[i].green = green;

        read_uint8(fin, &red);
        pbmp->palette[i].red = red;

        read_uint8(fin, &skipped_colors[i]);
    }

    uint32_t rounded_width = round_up(pbmp->width, 4);

    pbmp->a = calloc(width, sizeof(pbmp->a[0]));
    for (uint32_t j = 0; j < width; j++) {
        pbmp->a[j] = calloc(height, sizeof(pbmp->a[j][0]));
    }

    uint8_t val, trash;

    for (uint32_t y = 0; y < height; y++) {
        for (uint32_t x = 0; x < width; x++) {
            read_uint8(fin, &val);
            pbmp->a[x][y] = val;
        }

        for (uint32_t z = width; z < rounded_width; z++) {
            read_uint8(fin, &trash);
        }
    }

    return pbmp;
}

void bmp_write(const BMP *pbmp, FILE *fout) {
    uint32_t rounded_width = round_up(pbmp->width, 4);
    uint32_t image_size = pbmp->height * rounded_width;
    uint32_t file_header_size = 14;
    uint32_t bitmap_header_size = 40;
    uint32_t num_colors = MAX_COLORS;
    uint32_t palette_size = 4 * num_colors;
    uint32_t bitmap_offset = file_header_size + bitmap_header_size + palette_size;
    uint32_t file_size = bitmap_offset + image_size;

    write_uint8(fout, 'B');
    write_uint8(fout, 'M');
    write_uint32(fout, file_size);
    write_uint16(fout, 0);
    write_uint16(fout, 0);
    write_uint32(fout, bitmap_offset);
    write_uint32(fout, bitmap_header_size);
    write_uint32(fout, pbmp->width);
    write_uint32(fout, pbmp->height);
    write_uint16(fout, 1);
    write_uint16(fout, 8);
    write_uint32(fout, 0);
    write_uint32(fout, image_size);
    write_uint32(fout, 2835);
    write_uint32(fout, 2835);
    write_uint32(fout, num_colors);
    write_uint32(fout, num_colors);

    for (uint32_t i = 0; i < num_colors; i++) {
        write_uint8(fout, pbmp->palette[i].blue);
        write_uint8(fout, pbmp->palette[i].green);
        write_uint8(fout, pbmp->palette[i].red);
        write_uint8(fout, 0);
    }

    for (uint32_t y = 0; y < pbmp->height; y++) {
        for (uint32_t x = 0; x < pbmp->width; x++) {
            write_uint8(fout, pbmp->a[x][y]);
        }

        for (uint32_t z = pbmp->width; z < rounded_width; z++) {
            write_uint8(fout, 0);
        }
    }
}

void bmp_free(BMP **ppbmp) {
    for (uint32_t i = 0; i < (*ppbmp)->width; i++) {
        free((*ppbmp)->a[i]);
    }
    free((*ppbmp)->a);
    free(*ppbmp);
    *ppbmp = NULL;
}

uint8_t constrain(double x) {
    x = round(x);
    if (x < 0) {
        x = 0;
    }
    if (x > 255) {
        x = 255;
    }
    return (uint8_t) x;
}

void bmp_reduce_palette(BMP *pbmp) {
    uint8_t r, g, b, r_new, g_new, b_new;
    double SqLe, SeLq;
    for (uint32_t i = 0; i < MAX_COLORS; i++) {
        r = pbmp->palette[i].red;
        g = pbmp->palette[i].green;
        b = pbmp->palette[i].blue;

        SqLe = 0.00999 * r + 0.0664739 * g + 0.7317 * b;
        SeLq = 0.153384 * r + 0.316624 * g + 0.057134 * b;

        if (SqLe < SeLq) {
            r_new = constrain(0.426331 * r + 0.875102 * g + 0.0801271 * b);
            g_new = constrain(0.281100 * r + 0.571195 * g + -0.0392627 * b);
            b_new = constrain(-0.0177052 * r + 0.0270084 * g + 1.00247 * b);
        } else {
            r_new = constrain(0.758100 * r + 1.45387 * g + -1.48060 * b);
            g_new = constrain(0.118532 * r + 0.287595 * g + 0.725501 * b);
            b_new = constrain(-0.00746579 * r + 0.0448711 * g + 0.954303 * b);
        }

        pbmp->palette[i].red = r_new;
        pbmp->palette[i].green = g_new;
        pbmp->palette[i].blue = b_new;
    }
}

#include "io.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void read_uint8(FILE *fin, uint8_t *px) {
    int result = fgetc(fin);
    if (result == EOF) {
        fprintf(stderr, "unexpected end of file\n");
        exit(1);
    }
    *px = (uint8_t) result;
}

void read_uint16(FILE *fin, uint16_t *px) {
    uint8_t res1, res2;
    read_uint8(fin, &res1);
    read_uint8(fin, &res2);

    uint32_t temp = (res2 << 8) | res1;
    *px = temp;
}

void read_uint32(FILE *fin, uint32_t *px) {
    uint16_t res1, res2;
    read_uint16(fin, &res1);
    read_uint16(fin, &res2);

    uint32_t temp = (res2 << 16) | res1;
    *px = temp;
}

void write_uint8(FILE *fout, uint8_t x) {
    int result = fputc(x, fout);
    if (result == EOF) {
        fprintf(stderr, "unable to write file");
        exit(1);
    }
}

void write_uint16(FILE *fout, uint16_t x) {
    write_uint8(fout, x);
    write_uint8(fout, (x >> 8));
}

void write_uint32(FILE *fout, uint32_t x) {
    write_uint16(fout, x);
    write_uint16(fout, (x >> 16));
}

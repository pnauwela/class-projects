#include "bmp.h"
#include "io.h"

#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define OPTIONS ":i:o:h"

int main(int argc, char **argv) {
    int opt;

    char *input_file, *output_file;

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'i': input_file = optarg; break;
        case 'o': output_file = optarg; break;
        case 'h':
            printf("Usage: colorb -i infile -o outfile\n");
            printf("       colorb -h\n");
            return 0;
        default:
            fprintf(stderr, "Usage: colorb -i infile -o outfile\n");
            fprintf(stderr, "       colorb -h\n");
            return 1;
        }
    }
    if (argc == 1) {
        fprintf(stderr, "Usage: colorb -i infile -o outfile\n");
        fprintf(stderr, "       colorb -h\n");
        return 1;
    }

    //open input file
    FILE *fin = fopen(input_file, "rb");
    if (fin == NULL) {
        fprintf(stderr, "Fatal Error: could not open input file\n");
        return 1;
    }

    //create file in memory
    BMP *img = bmp_create(fin);

    //close input file
    if (fclose(fin) == EOF) {
        fprintf(stderr, "Fatal Error: could not close input file \n");
        return 1;
    }

    //reduce palette of colors
    bmp_reduce_palette(img);

    //open output file
    FILE *fout = fopen(output_file, "wb");
    if (fout == NULL) {
        fprintf(stderr, "Fatal Error: could not open output file\n");
        return 1;
    }

    //write to output file
    bmp_write(img, fout);

    //close output file
    if (fclose(fout) == EOF) {
        fprintf(stderr, "Fatal Error: could not close output file\n");
        return 1;
    }

    //free memory allocated
    bmp_free(&img);

    return 0;
}
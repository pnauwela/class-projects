#include "hangman_helpers.h"

int compare(const void *a,
    const void *
        b) { //CODE ADAPTED FROM https://www.ibm.com/docs/en/zos/2.3.0?topic=functions-qsort-sort-array#:~:text=The%20qsort()%20function%20sorts,is%20pointed%20to%20by%20base.
    const char *A = a, *B = b;
    return (*A > *B) - (*A < *B);
}

int main(int argc, const char **argv) {
    if (argc != 2) {
        fprintf(stderr, "wrong number of arguments\nusage: ./hangman <secret word or phrase>\nif "
                        "the secret is multiple words you must quote it");
    }
    if (!validate_secret(argv[1])) {
        return 1;
    }

    size_t len = strlen(argv[1]);
    //copy over secret word to the guess, then replace with _ for characters.
    //Later on I need to use this string and replace _ with the correctly guessed characters
    //doing this based on address? or the index of the characters as returned by that one searching function
    char gues[MAX_LENGTH] = { 0 }; //storage array for ______ guesses
    char elim[7] = { 0 }; //elimiated letters, max of 6
    strncpy(gues, argv[1], MAX_LENGTH); //copy over user secret to guess array
    char *chr = gues; //pointer to array
    int mistake_counter = 0;

    for (int k = 0; k < (int) (len); ++k) {
        if (*chr >= 'a' && *chr <= 'z') { //replace letters with _ in guess array
            *chr = '_';
            ++chr;
        } else {
            ++chr;
        }
    }

    do {
        char *win = strchr(gues, '_');
        if (!win) {
            printf("%s", CLEAR_SCREEN);
            printf("%s\n", arts[mistake_counter]);
            printf("\n");
            printf("    Phrase: %s\nEliminated: %s\n", gues, elim);
            printf("\nYou win! The secret phrase was: %s\n", gues);
            return 0;
        }

        printf("%s", CLEAR_SCREEN);
        printf("%s\n", arts[mistake_counter]);
        printf("\n");
        printf("    Phrase: %s\nEliminated: %s\n\n", gues, elim);

        char input = read_letter();
        while (!is_lowercase_letter(input)) {
            input = read_letter();
        }

        if (string_contains_character(argv[1], input)) {
            //do shit with the guess, find the index and all that bs
            char *s = strchr(argv[1], input);
            for (int i = 0; i < (int) (len); ++i) {
                if (s) {
                    gues[s - argv[1]] = input;
                    s = strchr(s + 1, input);
                }
            }
        } else {
            char *r = strchr(elim, input);
            if (!r) {
                ++mistake_counter;
                strncat(elim, &input, 1);
                qsort(elim, strlen(elim), sizeof(char), compare);
            }
        }
    } while (mistake_counter < LOSING_MISTAKE);
    printf("%s", CLEAR_SCREEN);
    printf("%s\n\n", arts[mistake_counter]);
    printf("    Phrase: %s\nEliminated: %s\n", gues, elim);
    printf("\nYou lose! The secret phrase was: %s\n", argv[1]);
    return 0;
}

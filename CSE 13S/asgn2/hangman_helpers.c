#include "hangman_helpers.h"

bool is_lowercase_letter(char c) {
    if ((c < 'a') || (c > 'z')) {
        return false;
    } else {
        return true;
    }
}

bool validate_secret(const char *secret) {
    size_t len = strlen(secret);
    if ((int) len > MAX_LENGTH) {
        return false;
    }
    for (int i = 0; i < (int) len; ++i) {
        if ((*secret >= 'a' && *secret <= 'z') || *secret == *punctuation
            || *secret == *(punctuation + 1) || *secret == *(punctuation + 2)) {
            ++secret;
        } else {
            fprintf(stdout,
                "invalid character: '%c'\nthe secret phrase must contain only lowercase "
                "letters, spaces, hyphens, and apostrophes\n",
                *secret);
            return false;
        }
    }
    return true;
}

bool string_contains_character(const char *s, char c) {
    char *t = strchr(s, c);
    if (!t) {
        return false;
    }
    return true;
}

char read_letter(void) {
    do {
        printf("Guess a letter: ");
        int guess = getchar();
        getchar();
        fflush(NULL);
        if (guess != '\n' && guess != EOF) {
            char c = (char) guess;
            return c;
        }
    } while (true);
}

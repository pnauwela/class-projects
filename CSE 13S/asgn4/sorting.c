#include "batcher.h"
#include "heap.h"
#include "insert.h"
#include "quick.h"
#include "set.h"
#include "shell.h"
#include "stats.h"

#include <getopt.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define OPTIONS      ":r:n:p:aishqbH"
#define INSERT_SORT  0
#define SHELL_SORT   1
#define HEAP_SORT    2
#define QUICK_SORT   3
#define BATCHER_SORT 4

int main(int argc, char **argv) {
    Stats *data = calloc(2, sizeof(Stats));
    reset(data);
    int opt;
    uint32_t rand_seed = 13371453;
    uint32_t prnt_len = 100;
    uint32_t arr_len = 100;
    Set opt_set = set_empty();

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'a':
            opt_set = set_insert(opt_set, INSERT_SORT);
            opt_set = set_insert(opt_set, SHELL_SORT);
            opt_set = set_insert(opt_set, HEAP_SORT);
            opt_set = set_insert(opt_set, QUICK_SORT);
            opt_set = set_insert(opt_set, BATCHER_SORT);
            break;
        case 'i': opt_set = set_insert(opt_set, INSERT_SORT); break;
        case 's': opt_set = set_insert(opt_set, SHELL_SORT); break;
        case 'h': opt_set = set_insert(opt_set, HEAP_SORT); break;
        case 'q': opt_set = set_insert(opt_set, QUICK_SORT); break;
        case 'b': opt_set = set_insert(opt_set, BATCHER_SORT); break;
        case 'r': rand_seed = (uint32_t) atoi(optarg); break;
        case 'n': arr_len = (uint32_t) atoi(optarg); break;
        case 'p': prnt_len = (uint32_t) atoi(optarg); break;
        case 'H':
            printf("Select at least one sort to perform.\n");
            printf("SYNOPSIS\n");
            printf("   A collection of comparison-based sorting algorithms.\n");
            printf("\n");
            printf("USAGE\n");
            printf("   ./sorting [-Hahbsqi] [-n length] [-p elements] [-r seed]\n");
            printf("\n");
            printf("OPTIONS\n");
            printf("   -H              Display program help and usage.\n");
            printf("   -a              Enable all sorts.\n");
            printf("   -h              Enable Heap Sort.\n");
            printf("   -b              Enable Batcher Sort.\n");
            printf("   -s              Enable Shell Sort.\n");
            printf("   -q              Enable Quick Sort.\n");
            printf("   -i              Enable Insertion Sort.\n");
            printf("   -n length       Specify number of array elements (default: 100).\n");
            printf("   -p elements     Specify number of elements to print (default: 100).\n");
            printf("   -r seed         Specify random seed (default: 13371453).\n");
            return 1;
        }
    }
    if (argc == 1) {
        fprintf(stderr, "Select at least one sort to perform.\n");
        fprintf(stderr, "SYNOPSIS\n");
        fprintf(stderr, "   A collection of comparison-based sorting algorithms.\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "USAGE\n");
        fprintf(stderr, "   ./sorting [-Hahbsqi] [-n length] [-p elements] [-r seed]\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "OPTIONS\n");
        fprintf(stderr, "   -H              Display program help and usage.\n");
        fprintf(stderr, "   -a              Enable all sorts.\n");
        fprintf(stderr, "   -h              Enable Heap Sort.\n");
        fprintf(stderr, "   -b              Enable Batcher Sort.\n");
        fprintf(stderr, "   -s              Enable Shell Sort.\n");
        fprintf(stderr, "   -q              Enable Quick Sort.\n");
        fprintf(stderr, "   -i              Enable Insertion Sort.\n");
        fprintf(stderr, "   -n length       Specify number of array elements (default: 100).\n");
        fprintf(stderr, "   -p elements     Specify number of elements to print (default: 100).\n");
        fprintf(stderr, "   -r seed         Specify random seed (default: 13371453).\n");
        return 1;
    }

    int *arr = NULL;
    int val;

    if (set_member(opt_set, INSERT_SORT) == 1) {
        srandom(rand_seed);
        arr = calloc(arr_len, sizeof(int));
        for (uint32_t i = 0; i < arr_len; i++) {
            arr[i] = ((uint32_t) random() & 0x3FFFFFFF);
        }
        insertion_sort(data, arr, arr_len);
        print_stats(data, "Insertion Sort", arr_len);
        val = 0;
        if (arr_len < prnt_len) {
            for (uint32_t x = 0; x < arr_len; x++) {
                if (x == 0) {
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                    continue;
                }
                if (val % 5 == 0) {
                    printf("\n");
                }
                printf("%13" PRIu32, arr[x]);
                val += 1;
            }
            printf("\n");
        } else {
            if (prnt_len != 0) {
                for (uint32_t x = 0; x < prnt_len; x++) {
                    if (x == 0) {
                        printf("%13" PRIu32, arr[x]);
                        val += 1;
                        continue;
                    }
                    if (val % 5 == 0) {
                        printf("\n");
                    }
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                }
                printf("\n");
            }
        }
        free(arr);
        arr = NULL;
        reset(data);
    }

    //Heap sort
    if (set_member(opt_set, HEAP_SORT) == 1) {
        srandom(rand_seed);
        arr = calloc(arr_len, sizeof(int));
        for (uint32_t i = 0; i < arr_len; i++) {
            arr[i] = ((uint32_t) random() & 0x3FFFFFFF);
        }
        heap_sort(data, arr, arr_len);
        print_stats(data, "Heap Sort", arr_len);
        val = 0;
        if (arr_len < prnt_len) {
            for (uint32_t x = 0; x < arr_len; x++) {
                if (x == 0) {
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                    continue;
                }
                if (val % 5 == 0) {
                    printf("\n");
                }
                printf("%13" PRIu32, arr[x]);
                val += 1;
            }
            printf("\n");
        } else {
            if (prnt_len != 0) {
                for (uint32_t x = 0; x < prnt_len; x++) {
                    if (x == 0) {
                        printf("%13" PRIu32, arr[x]);
                        val += 1;
                        continue;
                    }
                    if (val % 5 == 0) {
                        printf("\n");
                    }
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                }
                printf("\n");
            }
        }
        free(arr);
        arr = NULL;
        reset(data);
    }

    //shell
    if (set_member(opt_set, SHELL_SORT) == 1) {
        srandom(rand_seed);
        arr = calloc(arr_len, sizeof(int));
        for (uint32_t i = 0; i < arr_len; i++) {
            arr[i] = ((uint32_t) random() & 0x3FFFFFFF);
        }
        val = 0;
        shell_sort(data, arr, arr_len);
        print_stats(data, "Shell Sort", arr_len);

        if (arr_len < prnt_len) {
            for (uint32_t x = 0; x < arr_len; x++) {
                if (x == 0) {
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                    continue;
                }
                if (val % 5 == 0) {
                    printf("\n");
                }
                printf("%13" PRIu32, arr[x]);
                val += 1;
            }
            printf("\n");
        } else {
            if (prnt_len != 0) {
                for (uint32_t x = 0; x < prnt_len; x++) {
                    if (x == 0) {
                        printf("%13" PRIu32, arr[x]);
                        val += 1;
                        continue;
                    }
                    if (val % 5 == 0) {
                        printf("\n");
                    }
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                }
                printf("\n");
            }
        }
        free(arr);
        arr = NULL;
        reset(data);
    }

    //quick
    if (set_member(opt_set, QUICK_SORT) == 1) {
        srandom(rand_seed);
        arr = calloc(arr_len, sizeof(int));
        for (uint32_t i = 0; i < arr_len; i++) {
            arr[i] = ((uint32_t) random() & 0x3FFFFFFF);
        }
        quick_sort(data, arr, arr_len);

        print_stats(data, "Quick Sort", arr_len);
        val = 0;
        if (arr_len < prnt_len) {
            for (uint32_t x = 0; x < arr_len; x++) {
                if (x == 0) {
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                    continue;
                }
                if (val % 5 == 0) {
                    printf("\n");
                }
                printf("%13" PRIu32, arr[x]);
                val += 1;
            }
            printf("\n");
        } else {
            if (prnt_len != 0) {
                for (uint32_t x = 0; x < prnt_len; x++) {
                    if (x == 0) {
                        printf("%13" PRIu32, arr[x]);
                        val += 1;
                        continue;
                    }
                    if (val % 5 == 0) {
                        printf("\n");
                    }
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                }
                printf("\n");
            }
        }
        free(arr);
        arr = NULL;
        reset(data);
    }

    //batcher sort
    if (set_member(opt_set, BATCHER_SORT) == 1) {
        srandom(rand_seed);
        arr = calloc(arr_len, sizeof(int));
        for (uint32_t i = 0; i < arr_len; i++) {
            arr[i] = ((uint32_t) random() & 0x3FFFFFFF);
        }
        batcher_sort(data, arr, arr_len);
        print_stats(data, "Batcher Sort", arr_len);
        val = 0;
        if (arr_len < prnt_len) {
            for (uint32_t x = 0; x < arr_len; x++) {
                if (x == 0) {
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                    continue;
                }
                if (val % 5 == 0) {
                    printf("\n");
                }
                printf("%13" PRIu32, arr[x]);
                val += 1;
            }
            printf("\n");
        } else {
            if (prnt_len != 0) {
                for (uint32_t x = 0; x < prnt_len; x++) {
                    if (x == 0) {
                        printf("%13" PRIu32, arr[x]);
                        val += 1;
                        continue;
                    }
                    if (val % 5 == 0) {
                        printf("\n");
                    }
                    printf("%13" PRIu32, arr[x]);
                    val += 1;
                }
                printf("\n");
            }
        }
        free(arr);
        arr = NULL;
        reset(data);
    }
    free(data);
    return 0;
}

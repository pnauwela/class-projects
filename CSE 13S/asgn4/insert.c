#include "insert.h"

#include "stats.h"

void insertion_sort(Stats *stats, int *arr, int length) {
    int j, temp;
    for (int i = 1; i < length; i++) {
        j = i;
        temp = move(stats, arr[i]);
        while (j >= 1 && (cmp(stats, temp, arr[j - 1]) < 0)) {
            arr[j] = move(stats, arr[j - 1]);
            j -= 1;
        }
        arr[j] = move(stats, temp);
    }
}

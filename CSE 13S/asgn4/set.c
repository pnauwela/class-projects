#include "set.h"

#include <stdbool.h>

Set set_empty(void) {
    Set s = 0x00;
    return s;
}

Set set_universal(void) {
    Set s = 0xFF;
    return s;
}

bool set_member(Set s, int x) {
    if ((s & (0x01 << x)) != 0x00) {
        return true;
    }
    return false;
}

Set set_insert(Set s, int x) {
    s = (s | (0x01 << x));
    return s;
}

Set set_remove(Set s, int x) {
    s = (s & ~(0x01 << x));
    return s;
}

Set set_union(Set s, Set t) {
    Set u = s | t;
    return u;
}

Set set_intersect(Set s, Set t) {
    Set i = s & t;
    return i;
}

Set set_difference(Set s, Set t) {
    Set d = s & ~t;
    return d;
}

Set set_complement(Set s) {
    return ~s;
}

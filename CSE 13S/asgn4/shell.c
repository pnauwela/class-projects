#include "shell.h"

#include "gaps.h"
#include "stats.h"

void shell_sort(Stats *stats, int *A, int n) {
    int g, temp, j;
    for (int gap_count = 0; gap_count < GAPS; gap_count++) {
        g = gaps[gap_count];
        if (g > (n - 1)) {
            continue;
        } else {
            for (int i = g; i < n; i++) {
                j = i;
                temp = move(stats, A[i]);
                while (j >= g && (cmp(stats, temp, A[j - g]) < 0)) {
                    A[j] = move(stats, A[j - g]);
                    j -= g;
                }
                A[j] = move(stats, temp);
            }
        }
    }
}

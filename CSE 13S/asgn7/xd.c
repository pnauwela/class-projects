#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#define S    putchar(' ')
#define P    putchar('.')
#define N    putchar('\n')
#define D(H) (H < 32 || H > 126) ? P : C(H)
#define C(C) putchar(C)
#define I(N) printf("%08x: ", N)
#define B(N) printf("%02x", N)
int main(int argc, char *argv[]) {
    unsigned char b[16], ch;
    int i = 0, f, tb = 0, l, br;
    argc > 2 ? (f = -1) : (argc == 2 ? f = open(argv[1], O_RDONLY) : (f = 0));
    if (f < 0) {
        return 1;
    }
    while (1) {
        br = read(f, b + tb, (16 - tb));
        if (br <= 0) {
            break;
        }
        tb += br;
        if (tb == 16) {
            I(i);

            for (l = 0; l < 16; l++) {
                B(b[l]);
                if (l % 2 == 1) {
                    S;
                }
            }
            S;
            for (l = 0; l < 16; l++) {
                ch = b[l];
                D(ch);
            }
            N;
            i += 16;
            tb = 0;
        }
    }
    if (tb > 0) {
        I(i);
        for (l = 0; l < tb; l++) {
            B(b[l]);
            if (l % 2 == 1) {
                S;
            }
        }
        for (l = tb; l < 16; l++) {
            printf("  ");
            if (l % 2 == 1) {
                S;
            }
        }
        S;
        for (l = 0; l < tb; l++) {
            ch = b[l];
            D(ch);
        }
        N;
    }
    if (argc == 2) {
        close(f);
    }
    return 0;
}

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#define U    16
#define S    putchar(' ')
#define P    putchar('.')
#define N    putchar('\n')
#define D(H) (H < 32 || H > 126) ? P : C(H)
#define C(C) putchar(C)
#define I(N) printf("%08x: ", N)
#define B(N) printf("%02x", N)
int main(int argc, char *argv[]) {
    unsigned char b[U], ch;
    int i = 0, f, tb = 0, br, l;
    argc > 2 ? (f = -1) : (argc == 2 ? f = open(argv[1], O_RDONLY) : (f = 0));
    if (f < 0) {
        return 1;
    }
    while (1) {
        br = read(f, b + tb, (U - tb));
        if (br <= 0) {
            break;
        }
        tb += br;
        if (tb == U) {
            I(i);
            for (l = 0; l < U; l++) {
                B(b[l]);
                if (l % 2 == 1) {
                    S;
                }
            }
            S;
            for (l = 0; l < U; l++) {
                ch = b[l];
                D(ch);
            }
            N;
            i += U;
            tb = 0;
        }
    }
    if (tb > 0) {
        I(i);
        for (l = 0; l < tb; l++) {
            B(b[l]);
            if (l % 2 == 1) {
                S;
            }
        }
        for (int l = tb; l < U; l++) {
            printf("  ");
            if (l % 2 == 1) {
                S;
            }
        }
        S;
        for (l = 0; l < tb; l++) {
            ch = b[l];
            D(ch);
        }
        N;
    }
    if (argc == 2) {
        close(f);
    }
    return 0;
}

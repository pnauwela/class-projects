#include "names.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    //setup for pig sides
    typedef enum { SIDE, RAZORBACK, TROTTER, SNOUTER, JOWLER } Position;

    const Position pig[7] = {
        SIDE,
        SIDE,
        RAZORBACK,
        TROTTER,
        SNOUTER,
        JOWLER,
        JOWLER,
    };

    //user input for number of players
    int num_players = 2;
    printf("Number of players (2 to 10)? ");
    int scanf_result = scanf("%d", &num_players);

    if (scanf_result < 1 || num_players < 2 || num_players > 10) {
        fprintf(stderr, "Invalid number of players. Using 2 instead.\n");
        num_players = 2;
    }

    unsigned seed = 2023;
    printf("Random-number seed? ");
    scanf_result = scanf("%d", &seed);

    if (scanf_result < 1) {
        fprintf(stderr, "Invalid seed. Using 2023 instead.\n");
        seed = 2023;
    }

    srandom(seed);

    //player points tally and counter for player names
    int player_points[] = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    };
    int player_counter = 0;

    //set seed
    int side_selection, win;

    printf("%s\n", player_name[player_counter]);

    do {
        //roll that pig
        side_selection = random() % 7;

        switch (pig[side_selection]) {
        case 0:
            printf(" rolls 0, has %d\n", player_points[player_counter]);
            if (player_counter >= num_players - 1) {
                player_counter = 0;
            } else {
                ++player_counter;
            }
            printf("%s\n", player_name[player_counter]);
            break;
        case 1:
        case 2:
            player_points[player_counter] += 10;
            printf(" rolls 10, has %d\n", player_points[player_counter]);
            break;
        case 3:
            player_points[player_counter] += 15;
            printf(" rolls 15, has %d\n", player_points[player_counter]);
            break;
        case 4:
            player_points[player_counter] += 5;
            printf(" rolls 5, has %d\n", player_points[player_counter]);
            break;
        }

        if (player_points[player_counter] >= 100) {
            win = 1;
        } else {
            win = 0;
        }
    } while (win == 0);

    printf("%s won!\n", player_name[player_counter]);

    return 0;
}

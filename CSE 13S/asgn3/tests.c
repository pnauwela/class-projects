#include "mathlib.h"
#include "messages.h"
#include "operators.h"
#include "stack.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>

int main(void) {
    for (int i = 0; i < 60; i++) {
        printf("%.15f\n", tan(i));
    }
    stack_print();
    return 0;
}

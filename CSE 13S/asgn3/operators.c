#include "operators.h"

#include "mathlib.h"
#include "messages.h"
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>

double operator_add(double lhs, double rhs) {
    return (lhs + rhs);
}

double operator_sub(double lhs, double rhs) {
    return (lhs - rhs);
}

double operator_mul(double lhs, double rhs) {
    return (lhs * rhs);
}

double operator_div(double lhs, double rhs) {
    return (lhs / rhs);
}

bool apply_binary_operator(binary_operator_fn op) {
    if (stack_size < 2) {
        fprintf(stderr, ERROR_BINARY_OPERATOR);
        stack_clear();
        return false;
    }

    double rhs;
    double lhs;

    stack_pop(&rhs);
    stack_pop(&lhs);
    stack_push(op(lhs, rhs));
    return true;
}

bool apply_unary_operator(unary_operator_fn op) {
    if (stack_size < 1) {
        fprintf(stderr, ERROR_UNARY_OPERATOR);
        stack_clear();
        return false;
    }

    double x;

    stack_pop(&x);

    stack_push(op(x));
    return true;
}

bool parse_double(const char *s, double *d) {
    char *endptr;
    double result = strtod(s, &endptr);
    if (endptr != s) {
        *d = result;
        return true;
    } else {
        return false;
    }
}

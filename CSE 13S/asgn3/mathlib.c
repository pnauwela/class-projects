#include "mathlib.h"

#include <math.h>
#include <stdio.h>

double Abs(double x) {
    if (x < 0) {
        return ((-1) * x);
    }
    return x;
}

double Sqrt(double x) {
    if (x < 0) {
        return nan("nan");
    }

    double old = 0.0;
    double new = 1.0;

    while (Abs(old - new) > EPSILON) {
        old = new;
        new = 0.5 * (old + (x / old));
    }
    return new;
}

double Sin(double x) {
    x = fmod(x, (2.0 * M_PI));

    if (x == M_PI) {
        return (x / M_PI);
    }
    if (x == 0) {
        return x;
    }
    double n = 2.0;
    double fact_n = 2.0;
    double x_to_n = x * x;
    double return_value = x;
    double temp = x;

    while (1) {

        if (fmod(n, 4.0) == 1.0) {
            temp = ((1.0 * x_to_n) / fact_n);
        }
        if (fmod(n, 4.0) == 3.0) {
            temp = ((-1.0 * x_to_n) / fact_n);
        }

        if (Abs(temp) < EPSILON) {
            return ((return_value + temp) / 2.0);
        }
        n++;
        fact_n = fact_n * n;
        x_to_n = x_to_n * x;
        return_value += temp;
    }
}

double Cos(double x) {
    x = fmod(x, (2.0 * M_PI));

    if (x == 0) {
        x = 1.0;
        return x;
    }
    if (x == M_PI) {
        x = 0.0;
        return x;
    }
    double temp;
    double return_value = 1.0;
    double n = 1.0;
    double fact_n = (4 * (n * n) - (2 * n));
    double x_to_n = (x * x);

    while (1) {

        if (fmod(n, 2.0) == 0.0) {
            temp = (x_to_n / fact_n);
        } else {
            temp = ((-1) * x_to_n) / fact_n;
        }

        if (Abs(temp) < EPSILON) {
            return (return_value + temp);
        }

        n++;
        fact_n = fact_n * (4 * (n * n) - (2 * n));
        x_to_n *= (x * x);

        return_value += temp;
    }
}

double Tan(double x) {
    if (fmod(Abs(x), (2 * M_PI)) == 0) {
        x = 0.0;
        return x;
    }
    return (Sin(x) / Cos(x));
}

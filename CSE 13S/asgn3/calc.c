#include "mathlib.h"
#include "messages.h"
#include "operators.h"
#include "stack.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define OPTIONS "mh"

int main(int argc, char **argv) {
    int opt;
    const unary_operator_fn *unary = my_unary_operators;
    if (argc > 1) {

        while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
            switch (opt) {
            case 'm': unary = libm_unary_operators; break;
            case 'h': fprintf(stdout, USAGE, argv[0]); break;
            default:
                fprintf(stderr, "%s: invalid option -- '%s'\n", argv[0], argv[1]);
                fprintf(stderr, USAGE, argv[0]);
                return 1;
            }
        }
    }
    while (1) {
        fprintf(stderr, "> ");
        char *saveptr;
        bool error = false;

        char expr[1024] = { 0 };
        fgets(expr, 1024, stdin);
        strtok(expr, "\n");
        const char *token = strtok_r(expr, " ", &saveptr);

        if (feof(stdin) != 0) {
            stack_print();
            stack_clear();
            return 0;
        }
        while (token != NULL && !error) {
            double x;
            if (parse_double(token, &x)) {
                if (!stack_push(x)) {
                    stack_clear();
                    break;
                }
            } else if (strlen(token) > 1) {
                fprintf(stderr, ERROR_BAD_STRING, token);
                stack_clear();
                error = true;
            } else {
                switch ((int) *token) {
                case '+':
                case '-':
                case '*':
                case '/':
                case '%': apply_binary_operator(binary_operators[(int) *token]); break;
                case 's':
                case 'c':
                case 't':
                case 'a':
                case 'r': apply_unary_operator(*unary[(int) *token]); break;
                default:
                    fprintf(stderr, ERROR_BAD_CHAR, *token);
                    stack_clear();
                    error = true;
                }
            }

            token = strtok_r(NULL, " ", &saveptr);
        }
        stack_print();
        stack_clear();
    }

    return 0;
}

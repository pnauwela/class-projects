#include "bitreader.h"
#include "bitwriter.h"
#include "node.h"
#include "pq.h"

#include <getopt.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define OPTIONS ":i:o:h"
#define N(I)    Node *n##I = node_create(I, histogram[I]);
#define Q(I)    enqueue(q, n##I);
#define T(I)    Node *temp##I = node_create(0x00, left->weight + right->weight);
#define E(I)    enqueue(q, temp##I);
#define TL(I)   temp##I->left = left;
#define TR(I)   temp##I->right = right;

typedef struct Code {
    uint64_t code;
    uint8_t code_length;
} Code;

uint32_t fill_histogram(FILE *fin, uint32_t *histogram) {
    ++histogram[0x00];
    ++histogram[0xFF];
    uint32_t filesize = 0;
    char c = fgetc(fin);
    while (c != EOF) {
        ++histogram[c];
        c = fgetc(fin);
        filesize += 1;
    }
    return filesize;
}

Node *create_tree(uint32_t *histogram, uint16_t *num_leaves) {
    //node creation for non-zero weights
    PriorityQueue *q = pq_create();
    for (uint32_t i = 0; i < 256; i++) {
        if (histogram[i] != 0x00) {
            (*num_leaves)++;
            N(i);
            Q(i);
        }
    }
    int i = 500;
    //creating the main tree
    while (1) {
        if (pq_size_is_1(q)) {
            Node *r = dequeue(q);
            pq_free(&q);
            return r;
        } else {
            Node *left = dequeue(q);
            Node *right = dequeue(q);
            T(i);
            TL(i);
            TR(i);
            E(i);
            i++;
        }
    }
}

void fill_code_table(Code *code_table, Node *node, uint64_t code, uint8_t code_length) {
    if (node->left != NULL) {
        fill_code_table(code_table, node->left, code, (code_length + 1));

        code |= (uint64_t) 1 << code_length;
        fill_code_table(code_table, node->right, code, (code_length + 1));
    } else {
        code_table[node->symbol].code = code;
        code_table[node->symbol].code_length = code_length;
    }
}

void huff_write_tree(BitWriter *outbuf, Node *node) {
    if (node->left == NULL) {
        bit_write_bit(outbuf, 1);
        bit_write_uint8(outbuf, node->symbol);
    } else {
        huff_write_tree(outbuf, node->left);
        huff_write_tree(outbuf, node->right);
        bit_write_bit(outbuf, 0);
    }
}

void huff_compress_file(BitWriter *outbuf, FILE *fin, uint32_t filesize, uint16_t num_leaves,
    Node *code_tree, Code *code_table) {

    bit_write_uint8(outbuf, 'H');
    bit_write_uint8(outbuf, 'C');
    bit_write_uint32(outbuf, filesize);
    bit_write_uint16(outbuf, num_leaves);
    huff_write_tree(outbuf, code_tree);

    while (1) {
        char b = fgetc(fin);
        if (b == EOF) {
            break;
        }
        uint64_t code = code_table[b].code;
        uint8_t code_length = code_table[b].code_length;
        for (uint8_t i = 0; i < code_length; i++) {
            bit_write_bit(outbuf, (code & 1));
            code >>= 1;
        }
    }
}

int main(int argc, char **argv) {
    int opt;

    char *input_file, *output_file;

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'i': input_file = optarg; break;
        case 'o': output_file = optarg; break;
        case 'h':
            printf("Usage: huff -i infile -o outfile\n");
            printf("       huff -h\n");
            return 0;
        default:
            fprintf(stderr, "Usage: huff -i infile -o outfile\n");
            fprintf(stderr, "       huff -h\n");
            return 1;
        }
    }
    if (argc < 3) {
        fprintf(stderr, "Usage: huff -i infile -o outfile\n");
        fprintf(stderr, "       huff -h\n");
        return 1;
    }

    FILE *f = fopen(input_file, "rb");
    if (f == NULL) {
        fprintf(stderr, "Couldn't open input file\n");
        return 1;
    }

    BitWriter *bw = bit_write_open(output_file);

    uint32_t *histogram = calloc(256, sizeof(uint32_t));

    uint32_t filesize = fill_histogram(f, histogram);

    uint16_t leaves;

    Node *tree = create_tree(histogram, &leaves);
    node_print_tree(tree);
    fseek(f, 0, SEEK_SET);

    Code *ct[256];

    fill_code_table(ct, tree, 0, 0);

    huff_compress_file(bw, f, filesize, leaves, tree, ct);

    node_free(&tree);

    if (fclose(f) == EOF) {
        fprintf(stderr, "Error closing file\n");
        return 1;
    }
    bit_write_close(&bw);
    free(histogram);
    //free(ct);

    return 0;
}

#include "bitreader.h"
#include "node.h"

#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define OPTIONS ":i:o:h"

Node *stack_pop(Node *stack[64], uint8_t top) {
    Node *r = stack[top - 1];
    return r;
}

void stack_push(Node *n, Node *stack[64], uint8_t top) {
    stack[top] = n;
}

void dehuff_decompress_file(FILE *fout, BitReader *inbuf) {
    uint8_t stack_top = 0;
    Node *stack[64];
    uint8_t type1 = bit_read_uint8(inbuf);
    uint8_t type2 = bit_read_uint8(inbuf);
    uint32_t filesize = bit_read_uint32(inbuf);
    uint16_t num_leaves = bit_read_uint16(inbuf);

    uint16_t num_nodes = 2 * num_leaves - 1;
    Node *node;

    uint8_t bit, symbol;

    for (uint16_t i = 0; i < num_nodes; i++) {
        bit = bit_read_bit(inbuf);
        if (bit == 1) {
            symbol = bit_read_uint8(inbuf);
            node = node_create(symbol, 0);
        } else {
            node = node_create(0, 0);
            node->right = stack_pop(stack, stack_top);
            stack_top--;
            node->left = stack_pop(stack, stack_top);
            stack_top--;
        }
        stack_push(node, stack, stack_top);
        stack_top++;
    }
    Node *code_tree = stack_pop(stack, stack_top);
    stack_top--;

    for (uint32_t i = 0; i < filesize; i++) {
        node = code_tree;
        while (1) {
            bit = bit_read_bit(inbuf);
            if (bit == 0) {
                node = node->left;
            } else {
                node = node->right;
            }
            if (node->left == NULL && node->right == NULL) {
                break;
            }
        }
        fputc(node->symbol, fout);
    }
}

int main(int argc, char **argv) {
    int opt;

    char *input_file, *output_file;

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'i': input_file = optarg; break;
        case 'o': output_file = optarg; break;
        case 'h':
            printf("Usage: huff -i infile -o outfile\n");
            printf("       huff -h\n");
            return 0;
        default:
            fprintf(stderr, "Usage: dehuff -i infile -o outfile\n");
            fprintf(stderr, "       dehuff -h\n");
            return 1;
        }
    }
    if (argc < 3) {
        fprintf(stderr, "Usage: huff -i infile -o outfile\n");
        fprintf(stderr, "       huff -h\n");
        return 1;
    }

    FILE *f = fopen(output_file, "wb");
    if (f == NULL) {
        fprintf(stderr, "Couldn't open output file\b");
        return 1;
    }

    BitReader *b = bit_read_open(input_file);

    dehuff_decompress_file(f, b);

    fclose(f);
    bit_read_close(&b);
}

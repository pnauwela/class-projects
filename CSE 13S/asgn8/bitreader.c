#include "bitreader.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

struct BitReader {
    FILE *underlying_stream;
    uint8_t byte;
    uint8_t bit_position;
};

BitReader *bit_read_open(const char *filename) {
    BitReader *r = calloc(1, sizeof(BitReader));
    FILE *f = fopen(filename, "rb");
    r->underlying_stream = f;
    r->byte = 0x00;
    r->bit_position = 8; //set to 8 so it reads off the bat
    if (f == NULL) {
        fprintf(stderr, "BitReader error opening file for reading\n");
        return NULL;
    } else {
        return r;
    }
}

void bit_read_close(BitReader **pbuf) {
    if (*pbuf != NULL) {
        if (fclose((*pbuf)->underlying_stream) == EOF) {
            fprintf(stderr, "Error closing file in bitreader");
            exit(1);
        }
        free(*pbuf);
        *pbuf = NULL;
    }
}

uint8_t bit_read_bit(BitReader *buf) {
    if (buf->bit_position > 7) {
        buf->byte = fgetc(buf->underlying_stream);
        buf->bit_position = 0;
    }
    uint8_t bit = (buf->byte >> buf->bit_position) & 1;
    buf->bit_position += 1;
    return bit;
}

uint8_t bit_read_uint8(BitReader *buf) {
    uint8_t byte = 0x00;
    uint8_t b;
    for (int i = 0; i < 8; i++) {
        b = bit_read_bit(buf);
        byte = (b << i) | byte;
    }
    return byte;
}

uint16_t bit_read_uint16(BitReader *buf) {
    uint16_t word = 0x0000;
    uint8_t b = 0;
    for (int i = 0; i < 16; i++) {
        b = bit_read_bit(buf);
        word = (b << i) | word;
    }
    return word;
}

uint32_t bit_read_uint32(BitReader *buf) {
    uint32_t word = 0x00000000;
    uint8_t b = 0;
    for (int i = 0; i < 32; i++) {
        b = bit_read_bit(buf);
        word = (b << i) | word;
    }
    return word;
}

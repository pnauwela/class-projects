#include "pq.h"

#include "node.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct ListElement ListElement;

struct ListElement {
    Node *tree;
    ListElement *next;
};

struct PriorityQueue {
    ListElement *list;
};

PriorityQueue *pq_create(void) {
    PriorityQueue *pq = calloc(1, sizeof(PriorityQueue));
    return pq;
}

void pq_free(PriorityQueue **q) {
    if (*q != NULL) {
        free(*q);
        *q = NULL;
    }
    //check if I need to free the ListElement created in PriorityQueue
}

bool pq_is_empty(PriorityQueue *q) {
    if (q->list == NULL) {
        return true;
    } else {
        return false;
    }
}

bool pq_size_is_1(PriorityQueue *q) {
    if (q->list != NULL) {
        if (q->list->next == NULL) {
            return true;
        }
    }
    return false;
}

bool pq_less_than(ListElement *e1, ListElement *e2) {
    if (e1->tree->weight < e2->tree->weight) {
        return true;
    } else if (e1->tree->weight == e2->tree->weight) {
        if (e1->tree->symbol < e2->tree->symbol) {
            return true;
        }
    }
    return false;
}

void enqueue(PriorityQueue *q, Node *tree) {
    ListElement *new_element = calloc(1, sizeof(ListElement));
    new_element->tree = tree;

    if (pq_is_empty(q)) {
        q->list = new_element;
    } else if (pq_less_than(new_element, q->list)) {
        new_element->next = q->list;
        q->list = new_element;
    } else {
        ListElement *inter = q->list;
        while (1) {
            if (inter->next == NULL) {
                inter->next = new_element;
                return;
            }
            if (pq_less_than(new_element, inter->next)) {
                new_element->next = inter->next;
                inter->next = new_element;
                return;
            } else {
                inter = inter->next;
            }
        }
    }
}

Node *dequeue(PriorityQueue *q) {
    if (pq_is_empty(q)) {
        fprintf(stderr, "Priority Queue is empty, can't remove element\n");
        exit(1);
    }
    Node *r = q->list->tree;
    ListElement *e = q->list->next;
    free(q->list);
    q->list = e;
    return r;
}
void pq_print(PriorityQueue *q) {
    ListElement *e = q->list;
    int pos = 1;
    while (e != NULL) {
        if (pos++ == 1) {
            printf("=============================================\n");
        } else {
            printf("---------------------------------------------\n");
        }
        node_print_tree(e->tree);
        e = e->next;
    }
    printf("============================================\n");
    free(e);
}

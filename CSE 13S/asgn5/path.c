#include "path.h"

#include "graph.h"
#include "stack.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct path {
    uint32_t total_weight;
    Stack *vertices;
} Path;

Path *path_create(uint32_t capacity) {
    Path *p = calloc(1, sizeof(Path));

    p->total_weight = 0;
    p->vertices = stack_create(capacity);

    return p;
}

void path_free(Path **pp) {
    if (pp != NULL && *pp != NULL) {
        if ((*pp)->vertices) {
            free((*pp)->vertices);
            (*pp)->vertices = NULL;
        }
        (*pp)->total_weight = 0;
        free(*pp);
    }
    if (pp != NULL) {
        *pp = NULL;
    }
}

uint32_t path_vertices(const Path *p) {
    return stack_size(p->vertices);
}

uint32_t path_distance(const Path *p) {
    return p->total_weight;
}

void path_add(Path *p, uint32_t val, const Graph *g) {
    uint32_t prev_val;
    if (stack_peek(p->vertices, &prev_val)) {
        uint32_t w = graph_get_weight(g, prev_val, val);
        p->total_weight += w;
        stack_push(p->vertices, val);
    } else {
        stack_push(p->vertices, val);
    }
}

uint32_t path_remove(Path *p, const Graph *g) {
    uint32_t val;
    if (stack_size(p->vertices) > 1) {
        stack_pop(p->vertices, &val);
        uint32_t prev_val;
        stack_peek(p->vertices, &prev_val);
        uint32_t w = graph_get_weight(g, prev_val, val);
        p->total_weight -= w;
        return val;
    } else {
        stack_pop(p->vertices, &val);
        return val;
    }
}

void path_copy(Path *dst, const Path *src) {
    stack_copy(dst->vertices, src->vertices);
    dst->total_weight = src->total_weight;
}

void path_print(const Path *p, FILE *outfile, const Graph *g) {
    uint32_t len = path_vertices(p);
    uint32_t temp_path[len];
    for (uint32_t i = 0; i < len; i++) {
        stack_pop(p->vertices, &temp_path[i]);
    }
    for (uint32_t j = len; j >= 0; j--) {
        fprintf(outfile, "%s\n", graph_get_vertex_name(g, temp_path[j]));
        stack_push(p->vertices, temp_path[j]);
    }
}

#include "graph.h"
#include "path.h"
#include "stack.h"
#include "vertices.h"

#include <getopt.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OPTIONS ":i:odh"
/*
void dfs(Graph *g, Path *p, uint32_t n, uint32_t num) {
    graph_visit_vertex(g, n);
    path_add(p, n, g);
    for (uint32_t i = 0; i < num; i++) {
        if (graph_get_weight(g, n, i) != 0) {
            if (!graph_visited(g, i)) {
                dfs(g, p, i, num);
            }
        }
    }
    path_remove(p, g);
    graph_unvisit_vertex(g, n);
}*/

int main(int argc, char **argv) {
    int opt;
    //bool dash_i = false;
    //bool dash_o = false;
    //bool dash_d = false;
    //char output_file = '\0';
    //char input_file = '\0';

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'i':
            //dash_i = true;
            //input_file = *optarg;
            break;
        case 'o':
            //dash_o = true;
            //output_file = *optarg;
            break;
        case 'd': /*dash_d = true;*/ break;
        case 'h':
            printf("Usage: tsp [options]\n");
            printf("\n");
            printf("-i infile    Specify the input file path containing the cities and edges\n");
            printf("             of a graph. If not specified, the default input should be\n");
            printf("             set as stdin.\n\n");
            printf("-o outfile   Specify the output file path to print to. If not specified,\n");
            printf("             the default output should be set as stdout.\n\n");
            printf("-d           Specifies the graphs to be directed\n\n");
            printf("-h           Prints out a help message describing the purpose of the\n");
            printf("             graph and the command-line options it accepts, exiting the\n");
            printf("             program afterwards.\n");
            return 1;
        default:
            fprintf(stderr, "Usage: tsp [options]\n");
            fprintf(stderr, "\n");
            fprintf(stderr,
                "-i infile    Specify the input file path containing the cities and edges\n");
            fprintf(
                stderr, "             of a graph. If not specified, the default input should be\n");
            fprintf(stderr, "             set as stdin.\n\n");
            fprintf(stderr,
                "-o outfile   Specify the output file path to print to. If not specified,\n");
            fprintf(stderr, "             the default output should be set as stdout.\n\n");
            fprintf(stderr, "-d           Specifies the graphs to be directed\n\n");
            fprintf(
                stderr, "-h           Prints out a help message describing the purpose of the\n");
            fprintf(stderr,
                "             graph and the command-line options it accepts, exiting the\n");
            fprintf(stderr, "             program afterwards.\n");
            return 1;
        }
    }

    /*FILE *infile = stdin;
    if (dash_i) {
        infile = fopen(&input_file, "r");
        if (infile == NULL) {
            return 1;
        }
    }
    
    uint32_t num_vertices = 0;
    char name[] = "\0";

    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t k = 0;

    if (fscanf(infile, "%u\n", &num_vertices) != 1) {
        fprintf(stderr, "tsp: error reading number of vertices\n");
        exit(1);
    }

    Graph *g = graph_create(num_vertices, dash_d);

    for (uint32_t name_c = 0; name_c <= num_vertices; name_c++) {
        
        if (fscanf(infile, "%s\n", name) != 1) {
            fprintf(stderr, "tsp: error reading name\n");
            exit(1);
        } else {
            name[sizeof(name) - 1] = '\0';
            printf("%u\n", name_c);
            graph_add_vertex(g, name, name_c);
        }
    }

    uint32_t edge_cnt = 0;

    if (fscanf(infile, "%u", &edge_cnt) != 1) {
        fprintf(stderr, "tsp: error reading number of edges\n");
        exit(1);
    }

    for (uint32_t edges = 0; edges < num_vertices; edges++) {
        if (fscanf(infile, "%u %u %u", &i, &j, &k) != 3) {
            fprintf(stderr, "tsp: error reading weights\n");
        }
        graph_add_edge(g, i, j, k);
    }

    //close file
    if (dash_i) {
        fclose(infile);
    }

    //create path p and lowest path for lowest distance
    Path *p = path_create(num_vertices + 1);
    Path *lowest = path_create(num_vertices + 1);

    //call to dfs
    while (1) {
        dfs(g, p, START_VERTEX, num_vertices);
        if (path_distance(lowest) == 0) {
            path_copy(lowest, p);
        } else if (path_distance(p) < path_distance(lowest)) {
            path_copy(lowest, p);
        } else if (path_distance(p) == path_distance(lowest)) {
            break;
        }
    }

    //once dfs returns open output file

    FILE *outfile = stdout;
    if (dash_o) {
        outfile = fopen(&output_file, "w");
        if (outfile == NULL) {
            return 1;
        }
    }

    //write num_vertices
    fprintf(outfile, "Alissa starts at:\n");

    path_print(lowest, outfile, g);

    fprintf(outfile, "Total Distance: %u\n", path_distance(lowest));

    graph_free(&g);
    path_free(&p);
    path_free(&lowest);*/
    return 0;
}

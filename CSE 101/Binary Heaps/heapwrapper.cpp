#include "binheap.h"
#include <iostream>
#include <stack>
#include <stdexcept>
#include <fstream>
#include <array>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cstring>

using namespace std;

int main(int argc, char** argv) {
    if (argc < 3) {
        throw std::invalid_argument("Usage ./heapwrapper <INPUT FILE> <OUTPUT FILE>");
    }
    ifstream input;
    ofstream output;

    input.open(argv[1]);
    output.open(argv[2]);

    string command;
    char *com, *dummy, *valstr, *op;
    int val;

    BinHeap myHeap;

    while(getline(input, command)) {
        if (command.length() == 0) {
            continue;
        }
        com = strdup(command.c_str());
        op = strtok(com, " \t");

        if(strcmp(op, "p") == 0) {
            output << myHeap.print() << endl;
            cout << "Printing" << endl;
            cout << myHeap.print() << endl;
            continue;
        }
        valstr = strtok(NULL, " \t");
        if (valstr != NULL) {
            val = strtol(valstr, &dummy, 10);
        }
        if(strcmp(op, "i") == 0) {
            cout << "Insert "+to_string(val) << endl;
            myHeap.insert(val);
        }
        if (strcmp(op, "max") == 0) {
            int max = myHeap.findMax();
            output << to_string(max) << endl;
            cout << "Max: " + to_string(max) << endl;
        }
    }
}

#include "binheap.h"
#include <iostream>

int main() {
    BinHeap heap;

    heap.insert(20);
    heap.insert(18);
    heap.insert(25);
    heap.insert(15);
    heap.insert(5);
    heap.insert(12);
    heap.insert(10);

    heap.extractMax();
    
    cout << heap.print() << endl;

    return 0;
}
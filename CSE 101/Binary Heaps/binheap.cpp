#include "binheap.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

#define DEFAULT_CAPACITY 1000   //set default capacity

BinHeap :: BinHeap() {
    A = new int[DEFAULT_CAPACITY + 1];
    size = 0;
    capacity = DEFAULT_CAPACITY;
}

BinHeap :: BinHeap(int cap) {
    A = new int[DEFAULT_CAPACITY];
    size = 0;
    capacity = cap;
}

BinHeap :: BinHeap(int init[], int new_size, int cap) {
    A = init;
    size = new_size;
    capacity = cap;
    for (int i = size; i>0; i--) {
        A[i] = A[i-1];
    }
}

void BinHeap :: deleteHeap() {
    delete(A);
    return;
}

void BinHeap :: resize(int new_capacity) {
    if (new_capacity < DEFAULT_CAPACITY) {
        new_capacity = DEFAULT_CAPACITY;
    }
    int* old = A;
    A = new int[new_capacity + 1];
    capacity = new_capacity;
    for(int i = 1; i < size + 1; i++) {
        A[i] = old[i];
    }
    delete(old);
}

int BinHeap :: findMax() {
    return A[1];
}

void BinHeap :: swim(int index) {
    if (index < 1) {
        return;
    }
    if ((index == 1) || A[index] <= A[index/2]) {
        return;
    }
    int swap = A[index];
    A[index] = A[index/2];
    A[index/2] = swap;
    swim(index/2);
}

void BinHeap :: insert(int val) {
    if (size > capacity) {
        throw std::runtime_error("Error: size of heap is more than capacity\n");
    }
    if (size == capacity) {
        resize(2*capacity);
    }
    A[size + 1] = val;
    swim(size + 1);
    size++;
    return;
}

string BinHeap :: print() {
    string heap_str = "";
    if (size < 1) {
        return heap_str;
    }
    for (int i = 1; i < size + 1; i++) {
        heap_str = heap_str+to_string(A[i]) + " ";
    }
    heap_str.pop_back();
    return heap_str;
}

void BinHeap :: heapify(int i) {
    int left = 2 * i;
    int right = 2 * i + 1;
    int large = i;

    if(left < size && A[left] > A[large]) {
        large = left;
    } 
    if (right < size && A[right] > A[large]) {
        large = right;
    }

    if (large != i) {
        swap(A[i], A[large]);
        heapify(large);
    }
}

void BinHeap :: extractMax() {
    if (size < 1) {
        return;
    }
    A[1] = A[size--];
    heapify(1);
}
#ifndef HEAP_H
#define HEAP_H

#include <string>

using namespace std;

class BinHeap {
    private:
        int* A;         //array of ints
        int size;       //non negative size
        int capacity;   //non negative capacity
    public:
        BinHeap();      //default constructor
        BinHeap(int cap);   //constructor to set capacity
        BinHeap(int* init, int new_size, int cap);  //initialize the binary heap to an array
        void resize(int cap);   //resize underlying array to new capacity
        void deleteHeap();      //free array A

        int findMax();          //output max value
        void swim(int index);   //"swim" a high priority to the top of the heap
        void insert(int);       //insert int into heap
        string print();         //print the heap in array order into a string
        void extractMax();
        void heapify(int i);
};
#endif
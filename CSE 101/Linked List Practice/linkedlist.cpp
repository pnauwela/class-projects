#include "linkedlist.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

//Default constructor sets head to NULL (nothing in list yet)
LinkedList :: LinkedList() {
    head = NULL;
}

//inset val to head of list 
//Input: int to insert
//Output: no return 
void LinkedList :: insert(int val) {
    Node *to_add = new Node;    //Create new node
    to_add->data = val;         //Assign new node its value 

    to_add->next = head;        //point node at current head of list
    head = to_add;              //reassign head to new node 
}   

Node* LinkedList :: find(int val) {
    Node *curr = head;      //temp node to step through list

    while(curr != NULL) {
        if (curr->data == val) 
            return curr;        //if node has same value as input, return it
        curr = curr->next;      //step to next node if not
    }
    return NULL;        //if no match, return null
}   

Node* LinkedList:: deleteNode(int val) {
    Node* prev = NULL;
    Node* curr = head;
    while(curr != NULL) {
        if(curr->data == val) 
            break;
        prev = curr;
        curr = curr->next;
    }
    if (curr == NULL) 
        return NULL;
    if (prev == NULL)
        head = head->next;
    else 
        prev->next = curr->next;
    curr->next = NULL;
    return curr;
}

void LinkedList :: deleteAndFreeNode(int val) {
    Node* ptr = deleteNode(val);    //grab the value out of the list to delete
    if (ptr != NULL) {
        ptr->next = NULL;           //set next to null
        delete(ptr);                //free memory   
    }
    return;                         //return from function
}

void LinkedList :: deleteList() {
    Node *curr = head;      //point at start
    Node *temp = NULL;      //empty temp

    while (curr != NULL) {
        temp = curr->next;  //point temp to next
        delete(curr);       //delete current
        curr = temp;        //make current point to the temp (what was the next one)
    }
    head = NULL;            //Empty head
}

void LinkedList :: circular()
{
    Node *curr = head;
    while (curr->next != NULL)
    {
        curr = curr->next;
    }
    curr->next = head;
}

string LinkedList :: print() {
    string list_str = "";   //string
    Node *curr = head;      //point to head
    int count = 0;

    while((count < 10) && (curr != NULL)) {
        list_str = list_str + to_string(curr->data) + " ";  //append number and space
        curr = curr->next;      //point to next
        count++;
    }
    if (list_str.length() > 0)
        list_str.pop_back();    //remove space at end of list
    return list_str;            //go back
}

int LinkedList :: length() {
    int length = 0;
    Node *curr = head;

    while(curr != NULL) {
        length++;
        curr = curr->next;
    }
    return length;
}
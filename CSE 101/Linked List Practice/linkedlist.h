//Header file frome example project

#ifndef LIST_H
#define LIST_H

#include <string>

using namespace std;

//Node structure with data and next node pointer
struct Node {
    int data;
    Node *next;
};

class LinkedList {
    private:
        Node *head;                     //Pointer to head node (start of list)
    public:
        LinkedList();                   //Constructor 
        void insert(int);               //insertion method 
        Node* find(int);                //search for node 
        Node *deleteNode(int);          //remove a node
        void deleteAndFreeNode(int);    //frees the memory of a node and deletes it
        void deleteList();              //Delete every node in list 
        void circular();
        string print();                 //print whole list
        int length();                   //return length of list
};

#endif
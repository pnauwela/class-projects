#include "linkedlistfunc.h"
#include <iostream>

int main() {
    LinkedList list;

    list.insert(10);
    list.insert(9);
    list.insert(8);
    list.insert(7);
    list.insert(6);
    list.insert(5);
    list.insert(4);
    list.insert(3);
    list.insert(2);
    list.insert(1);

    cout << list.print() << endl;

    //list.dedup();
    list.reverseTail(4);

    cout << list.print() << endl;

    list.deleteList();

    return 0;
}
#include "linkedlistfunc.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

LinkedList :: LinkedList() 
{
    head = NULL;
}

//same as the original definiton b/c no need for recursion
void LinkedList :: insert(int val) 
{
    Node *to_add = new Node;
    to_add->count = val; 
    to_add->next = NULL;

    to_add->next = head;
    head = to_add;
}

//helper function for bubble sort algorithm (link to source/inspiration in README)
Node* swap(Node* node1, Node* node2)
{
  //temp to store overwritten node 
  Node* temp = node2->next;
  //point node2 at node 1
  node2->next = node1;
  //point node1 where node2 was
  node1->next = temp;
  //return the first node in order (push it to next in the bubble sort function)
  return node2;
}

void LinkedList :: insert_sort(int length) 
{
  //list empty, don't do anything
  if (head == NULL)
    return;

  //temp to push sorter forward
  Node** temp;

  for(int i = 0; i < length; i++)
  {
    temp = &head;
    for(int j = 0; j < length - i - 1; j++)
    {
      Node *t1 = *temp;
      Node *t2 = t1->next;

      if(t1->count > t2->count)
        *temp = swap(t1, t2);
      temp = &(*temp)->next;
    }
  }
}

Node* LinkedList :: find(int val) 
{
    return find(val, head); //call recursive function on head
}

Node* LinkedList :: find(int val, Node *curr) 
{
    if (curr == NULL)    //base case, list is empty
        return NULL;
    if (curr->count == val)  //current has val
        return curr;
    else 
        return find(val, curr->next);   //recursive call on next node
}

Node* LinkedList :: deleteNode(int val) 
{
    Node *temp = head;

    if (temp == NULL)
        return NULL;
    if (val == 0)
    {
        head = temp->next;
        return temp;
    }
    for (int i = 0; (temp != NULL) && (i < val - 1); i++)
    {
        temp = temp->next;
    }
    if ((temp == NULL) || (temp->next == NULL))
        return NULL;
    Node *next = temp->next->next;
    Node *to_return = temp->next;

    temp->next = next;
    return to_return;
}

void LinkedList :: deleteList()
{
    deleteList(head);
}

void LinkedList :: deleteList(Node *curr)
{
    if (curr == NULL) 
        return;
    Node *nextNode = curr->next;    //store next
    delete(curr);                   //delete current
    deleteList(nextNode);           //continue on with next node
}

string LinkedList :: print()
{
    string to_return = print(head);
    if (to_return.length() > 0)
        to_return.pop_back();
    return to_return;
}

string LinkedList :: print(Node *curr)
{
    if (curr == NULL)
        return "";
    return to_string(curr->count) + " " + print(curr->next);
}

int LinkedList :: length() 
{
    return length(head);
}

int LinkedList :: length(Node *curr)
{
    if (curr == NULL)
        return 0;
    return 1 + length(curr->next);
}

void LinkedList :: dedup() {
    Node *p1, *p2, *dup;

    p1 = head;

    while (p1 != NULL && p1->next != NULL) {
        p2 = p1;

        while (p2->next != NULL) {
            if (p1->count == p2->next->count) {
                dup = p2->next;
                p2->next = p2->next->next;
                delete(dup);
            } else {
                p2 = p2->next;
            }
        }
        p1 = p1->next;
    }
}

void LinkedList :: deleteLast(int val) {
    Node *match = NULL, *curr = head;

    while(curr) {
        if (curr->count == val) {
            match = curr;
        }
        curr = curr->next;
    }

    if (match != NULL && match->next == NULL) {
        curr = head;
        while(curr->next != match) {
            curr = curr->next;
        }
        curr->next = NULL;
    }

    if (match != NULL && match->next != NULL) {
        match->count = match->next->count;
        curr = match->next;
        match->next = match->next->next;
        delete(curr);
    }
}

void LinkedList :: rotate(int x) {
    if (x == 0) 
        return;

    Node *curr = head;

    int c = 1;
    while(c < x && curr != NULL) {
        curr = curr->next;
        c++;
    }
    if (curr == NULL)
        return;
    
    Node *kNode = curr;

    while(curr->next != NULL) {
        curr = curr->next;
    }

    curr->next = head;
    head = kNode->next;
    kNode->next = NULL;
}

void LinkedList :: rotateToHead(int x) {
    if (!head) 
        return;
    
    Node *curr = head;
    Node *prev = NULL;

    while(curr && curr->count != x) {
        prev = curr;
        curr = curr->next;
    }

    if (!curr || curr == head) 
        return;

    prev->next = curr->next;
    curr->next = head;
    head = curr;
}

void LinkedList :: reverseTail(int k) {
    if (!head || k <= 1) return;

    Node *dummy;
    dummy->next = head;

    Node *slow = dummy;
    Node *fast = dummy;

    for (int i = 0; i < k; i++) {
        if (!fast->next) return;
        fast = fast->next;
    }
    while(fast->next) {
        slow = slow->next;
        fast = fast->next;
    }

    Node *prev = NULL;
    Node *curr = slow->next;

    while(curr) {
        Node *next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }

    slow->next->next = curr;
    slow->next = prev;
}

bool LinkedList :: palindrome() {
    vector<int> A, B;
    Node *curr = head;

    while (curr != NULL) {
        A.push_back(curr->count);
        B.push_back(curr->count);
        curr = curr->next;
    }    
    reverse(A.begin(), A.end());
    return (A == B);
}
//Header file frome example project

#ifndef LIST_H
#define LIST_H

#include <string>

using namespace std;

//Node structure with data and next node pointer
struct Node {
    int data;
    int count;
    string word;
    Node *next;
};

class LinkedList {
    private:
        Node *head;                     //Pointer to head node (start of list)
    public:
        LinkedList();                   //Constructor 
        void insert(int);               //insertion method 
        void insert_sort(int);
        Node* find(int);                //search for node 
        Node* find(int, Node*);
        Node* deleteNode(int);          //remove a node
        Node* deleteNode(int, Node*, Node*);
        void deleteList();              //Delete every node in list 
        void deleteList(Node*);              //Delete every node in list 
        string print();                 //print whole list
        string print(Node*);
        int length();                   //return length of list
        int length(Node*);

        void dedup();
        void deleteLast(int);
        void reverseTail(int);
        void rotateToHead(int);
        void rotate(int);
        bool palindrome();
};

#endif
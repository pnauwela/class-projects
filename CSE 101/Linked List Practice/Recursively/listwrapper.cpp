//main file with all the code for doing things

#include "linkedlistfunc.h"

#include <iostream>
#include <stack>
#include <stdexcept>
#include <fstream>
#include <array>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cstring>

using namespace std;

int main(int argc, char** argv) 
{
    if (argc < 3) //if less than 3 arguments (call to execute and two files) error
    {
        throw invalid_argument("Usage: ./hello <INPUT FILE> <OUTPUT FILE>");
    }

    //setting input and output streams
    ifstream input;     
    ofstream output;

    //open files
    input.open(argv[1]);
    output.open(argv[2]);

    //create blank variables
    string command;
    char *com, *dummy, *valstr, *op;
    int val;

    //initialize the linked list as empty
    LinkedList myList[3];

    //looping through input file
    while(getline(input, command))
    {
        if (command.length() == 0) //empty command
            continue;
        com = strdup(command.c_str());  //copy into a c-style string
        op = strtok(com, " \t");    //tokenize command on whitespce

        //print command first 
        if (strcmp(op,"p") == 0) //if command is print then print list 
        {
            output << myList[0].print() << endl;
            cout << "Printing" << endl;
            cout << myList[0].print() << endl;
            continue;
        }

        valstr = strtok(NULL, " \t");   //next token is a value
        if(valstr != NULL) 
            val = strtol(valstr, &dummy, 10);   //convert initial portion to an integer value

        if (strcmp(op, "i") == 0)   //insert command
        {
            cout << "Insert " + to_string(val) << endl;
            myList[0].insert(val);
            //myList[0].insert_sort(myList[0].length());
        }
        if (strcmp(op, "d") == 0)   //delete from list  
        {
            cout << "Delete " + to_string(val) << endl;
            myList[0].deleteNode(val);
            //would I need to delete curr?
        }
        if (op != NULL)
            delete(op);     //free memory of string
    }
    myList[0].deleteList();
    input.close();
    output.close();
}
#ifndef LIST_H
#define LIST_H

#include <string>

using namespace std;

struct Node
{
    string word;
    int count;
    Node *next;
};

class LinkedList
{
private:
    Node *head;

public:
    LinkedList();
    void insert(string);
    void sort_list(int);      // step through list checking next until it's null or larger.
    Node *find(string);       // locate node for duplicate words - LinkedList is
    string find(int);         // locate string given commonality int k
    Node *deleteNode(string); // might not be needed
    void deleteList();
    void deleteList(Node *);
    string print();
    string print(Node *);
    int length(); // use to be sure that input isn't longer than list
};

#endif
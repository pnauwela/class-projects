#include "linkedlist.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

using namespace std;

int main(int argc, char **argv)
{
    if (argc < 3)
        throw invalid_argument("Usage: ./bard <INPUT FILE> <OUTPUT FILE>");
    ifstream input;
    ifstream shakespeare;
    ofstream output;

    shakespeare.open("shakespeare-cleaned5.txt");
    input.open(argv[1]);
    output.open(argv[2]);

    string word;

    LinkedList listLists[40];

    while (getline(shakespeare, word))
    {
        // if nothing at line, skip to next word
        if (word.length() == 0)
            continue;
        // if there's a newline at end, remove it
        if ((word[word.length() - 1]) == '\n')
            word.erase(word.length() - 1);

        // Check if string is already in a list
        Node *temp = listLists[word.length() - 5].find(word);
        if (temp == NULL)
            listLists[word.length() - 5].insert(word);
        else
            temp->count++;
    }
    shakespeare.close();

    // sort every list
    for (int i = 0; i < 40; i++)
    {
        if (listLists[i].length() == 0)
            continue;
        else
            listLists[i].sort_list(listLists[i].length());
    }

    // take input file and write to output result
    string numbers;
    int val1, val2;

    while (getline(input, numbers))
    {
        // empty line
        if (numbers.length() == 0)
            continue;

        // open linestream
        stringstream lineStream(numbers);
        // write from linestream to the two int values, ignores witespace
        lineStream >> val1 >> val2;
        cout << to_string(val2) << endl;
        cout << to_string(listLists[val1 - 5].length()) << endl;
        // if val2 is longer than list, or listlength is zero
        if ((listLists[val1 - 5].length() == 0) || (val2 > listLists[val1 - 5].length()) - 1)
        {
            // write a - character representing no word
            output << "-" << endl;
        }
        else
        {
            // write word of length val (5-val for offset b/c no word shorter than 5) at position val2
            output << listLists[val1 - 5].find(val2) << endl;
        }
    }

    // close input and output
    input.close();
    output.close();

    // delete lists if length is greater than 0
    for (int j = 0; j < 40; j++)
    {
        if (listLists[j].length() > 0)
            listLists[j].deleteList();
    }
}

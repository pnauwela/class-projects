#include "linkedlist.h"
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

LinkedList ::LinkedList()
{
    head = NULL;
}

void LinkedList ::insert(string word)
{
    Node *to_add = new Node;
    to_add->word = word;
    to_add->count = 1;
    to_add->next = head;
    head = to_add;
}

// helper function for bubble sort algorithm (link to source/inspiration in README)
Node *swap(Node *node1, Node *node2)
{
    // temp to store overwritten node
    Node *temp = node2->next;
    // point node2 at node 1
    node2->next = node1;
    // point node1 where node2 was
    node1->next = temp;
    // return the first node in order (push it to next in the bubble sort function)
    return node2;
}

// LinkedList gives access to head of whichever linked list is called
// call using listoflists[n].insert_sort(listoflists[n].length())
void LinkedList ::sort_list(int length)
{
    // list empty, don't do anything
    if (head == NULL)
        return;

    // temp to push sorter forward
    Node **temp;

    for (int i = 0; i < length; i++)
    {
        temp = &head;
        for (int j = 0; j < length - i - 1; j++)
        {
            Node *t1 = *temp;
            Node *t2 = t1->next;

            // if count of first node is less than next node swap it
            if (t1->count < t2->count)
                *temp = swap(t1, t2);
            else if (t1->count == t2->count)
            {
                // if count is same, check if word value is less than next. if it is then swap it
                if (t1->word > t2->word)
                    *temp = swap(t1, t2);
            }

            temp = &(*temp)->next;
        }
    }
}

Node *LinkedList ::find(string word)
{
    Node *curr = head;

    while (curr != NULL)
    {
        if (curr->word == word)
            return curr;
        curr = curr->next;
    }
    return NULL;
}

string LinkedList ::find(int rank)
{
    Node *curr = head;
    for (int i = 0; i < rank; i++)
    {
        curr = curr->next;
    }
    return curr->word;
}

Node *LinkedList ::deleteNode(string word)
{
    Node *prev = NULL;
    Node *curr = head;
    while (curr != NULL)
    {
        if (curr->word == word)
            break;
        prev = curr;
        curr = curr->next;
    }

    if (curr == NULL) // not in list
        return NULL;
    if (prev == NULL) // head of list deleted
        head = head->next;
    else
        prev->next = curr->next; // overwrite current node in list

    curr->next = NULL; // set to null for error preventions
    return curr;       // return deleted node for mem clearing
}

void LinkedList ::deleteList()
{
    deleteList(head);
}

void LinkedList ::deleteList(Node *curr)
{
    if (curr == NULL)
        return;
    Node *nextNode = curr->next;
    delete (curr);
    deleteList(nextNode);
}

string LinkedList ::print()
{
    string to_return = print(head);
    if (to_return.length() > 0)
        to_return.pop_back();
    return to_return;
}

string LinkedList ::print(Node *curr)
{
    if (curr == NULL)
        return "";
    return curr->word + " " + to_string(curr->count) + " " + print(curr->next);
}

int LinkedList ::length()
{
    int length = 0;
    Node *curr = head;

    while (curr != NULL)
    {
        length++;
        curr = curr->next;
    }
    return length;
}

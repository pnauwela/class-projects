#include "bst.h"
#include <iostream>

int main() {
    BST tree;

    tree.insert(6);
    tree.insert(2);
    tree.insert(8);
    tree.insert(0);
    tree.insert(4);
    tree.insert(7);
    tree.insert(3);
    tree.insert(5);

    Node* leastcommona = tree.lca(3, 5);

    if (leastcommona) {
        cout << "LCA " << leastcommona->key << endl;
    }
    cout << tree.printInOrder() << endl;

    cout << tree.width() << endl;

    return 0;
}

#include "bst.h"
#include <iostream>
#include <stack>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <array>
#include <algorithm>
#include <cstring>
#include <sstream>

using namespace std;

int main(int argc, char** argv) {
    if (argc < 3) {
        throw std::invalid_argument("usage: ./treewrapper <input> <output>");
    }

    ifstream input;
    ofstream output;

    input.open(argv[1]);
    output.open(argv[2]);

    string command;

    char *com, *dummy, *valstr, *op;
    int val = 0;

    BST myBST;

    while(getline(input, command)) {
        if (command.length() == 0) {
            continue;
        }
        com = strdup(command.c_str());
        op = strtok(com, " \t");

        valstr = strtok(NULL, " \t");
        if (valstr != NULL) {
            val = strtol(valstr, &dummy, 10);
        }
        if (strcmp(op, "i") == 0) {
            cout << "Insert " + to_string(val) << endl;
            myBST.insert(val);
        }
        if (strcmp(op, "d") == 0) {
            cout << "Delete " + to_string(val) << endl;
            free(myBST.deleteKey(val));
        }
        if (strcmp(op, "f") == 0) {
            string message;
            if (myBST.find(val)) {
                message = " found";
            } else {
                message = " not found";
            }
            cout << to_string(val) + message << endl;
            output << to_string(val) + message << endl;
        }
        if (strcmp(op, "pin") == 0) {
            output << myBST.printInOrder() << endl;
            cout << "Print in order" << endl;
            cout << myBST.printInOrder() << endl;
            continue;
        }
        if (strcmp(op, "ppre") == 0) {
            output << myBST.printPreOrder() << endl;
            cout << "Printing Pre Order" << endl;
            cout << myBST.printPreOrder() << endl;
            continue;
        }
        if (strcmp(op, "ppost") == 0) {
            output << myBST.printPostOrder() << endl;
            cout << "Printing post order" << endl;
            cout << myBST.printPostOrder() << endl;
            continue;
        }
    }
    myBST.deleteBST();
    input.close();
    output.close();
}
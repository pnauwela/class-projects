#include "bst.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cstring>
#include <sstream>

using namespace std;

int main(int argc, char** argv) {
    if (argc < 3) {
        cout << "Usage: ./wordrange <input> <output>" << endl;
        return 0;
    }

    ifstream input;
    ofstream output;

    input.open(argv[1]);
    output.open(argv[2]);

    string command;
    

    BST myBST;

    while(getline(input, command)) {
        if (command.length() == 0) {
            continue;
        }
        stringstream iss(command);
        char com;

        iss >> com;
        
        if (com == 'i') {
            int in;
            iss >> in;
            myBST.insert(in);
        }
        if (com == 'r') {
            int val1, val2;
            iss >> val1 >> val2;
            int result = myBST.compare(val1, val2);
            output << to_string(result) << endl;
        }
    }
    myBST.deleteBST();
    input.close();
    output.close();
}
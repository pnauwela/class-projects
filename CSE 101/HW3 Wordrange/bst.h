#ifndef BST_H
#define BST_H

#include <string>

using namespace std;

class Node {
    public:
        int key;
        Node *left, *right, *parent;

        Node() {
            left = right = parent = NULL;
        }

        Node(int val) {
            key = val;
            left = right = parent = NULL;
        }
};

class BST {
    private:
        Node* root;
    public:
        BST();                              //Default constructor, set root to NULL
        void insert(int);                   //insert int into list
        void insert(Node*, Node*);          //recrsive version that inserts a node
        Node* find(int);                    //find int in tree
        Node* find(Node*, int);             //recursive version that finds int in rooted subtree
        Node* minNode(Node*);               //find min node in subtree
        Node* maxNode(Node*);               //find max node in subtree
        Node* deleteKey(int);               //remove node with int if it exists and return pointer 
        Node* deleteNode(Node*);            //try to delete node pointed at
        void deleteBST();                   //delete entire tree
        void deleteBST(Node* start);        //deletes every node rooted at start
        string printInOrder();              //construct a string with tree printed in order
        string printInOrder(Node* start);   //string with subtree rooted at start
        string printPreOrder();             //string with tree printd pre ordered
        string printPreOrder(Node* start);  //string with subtree rooted at start printed pre order
        string printPostOrder();            //string with string printed in post order
        string printPostOrder(Node* start); //string with subtree rooted at start printed in post order;

        Node* lca(int, int);                //getting lowest common ancestors of two int

        int width();

        int compare(int, int);
};

#endif 
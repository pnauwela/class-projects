#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

bool safe(vector<pair<int, int>>& queens, int col, int row) {
  for (const auto& queen : queens) {
    int qCol = queen.first;
    int qRow = queen.second;
    if (qRow == row || qCol == col || abs(qRow - row) == abs(qCol - col)) {
      return false;
    }
  }
  return true;
}

pair<int, int> place(vector<pair<int, int>>& queens, int col, int start, int n) {
  for (int row = start; row <= n; row++) {
    if (safe(queens, col, row)) {
      return {col, row};
    }
  }
  return {-1, -1};
}

void nQueens(int n, vector<pair<int, int>>& queens, ofstream& output) {
  stack<pair<int, int>> position;
  for (const auto& queen : queens) {
    position.push(queen);
  }
  bool back = false;

  while(!position.empty()) {
    pair<int, int> curr = position.top();
    position.pop();

    int currCol = curr.first;
    int currRow = curr.second;

    int nextCol = currCol + 1;
    if (nextCol > n) {
      nextCol = 1;
    }

    bool skip = false;
    for (const auto& queen : queens) {
      if (queen.first == nextCol) {
        skip = true;
        break;
      }
    }
    if (skip) {
      continue;
    }
    if (back == false) {
      currRow = 1;
      cout << currRow << endl;
    }

    pair<int, int> nextPos = place(queens, nextCol, currRow, n);
    cout << nextPos.first << " " << nextPos.second << endl;
    if (nextPos.first != -1) {
      queens.push_back(nextPos);
      position.push(nextPos);
      back = false;

      if (queens.size() == n) {
        for (const auto& queen : queens) {
          output << queen.first << " " << queen.second << " ";
        }
        output << endl;
        return;
      }
    } else {
      back = true;
    }
  }
  output << "No solution" << endl;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    cout << "invalid arguments" << endl;
    return 0;
  }
  //do input handling stuff
  ifstream input;
  ofstream output;

  string raw;
  input.open(argv[1]);
  output.open(argv[2]);

  while (getline(input, raw)) {
    vector<pair<int, int>> pairs;
    int n;  //first at input


    istringstream iss(raw);
    iss >> n;

    int set_col, set_row;
    while(iss>>set_col && iss>>set_row) {
      pairs.push_back({set_col, set_row});
    }
    nQueens(n, pairs, output);
  }
  input.close();
  output.close();
  return 0;
}
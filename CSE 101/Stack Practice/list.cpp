#include "list.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

List :: List() {
    head = NULL;
}

void List :: insert(string s) {
    Node *latest = new Node(s);
    latest->next = head;
    head = latest;
    return;
}

Node* List :: find(string s) {
    Node *curr = head;
    
    while(curr != NULL) {
        if (curr->word == s) 
            return curr;
        curr = curr->next;
    }

    return NULL;
}

void List :: sort() {
    Node *curr1 = head, *curr2 = head;
    Node *min;
    string temp_word;

    while(curr1 != NULL) {
        curr2 = curr1;
        min = curr2;
        while (curr2 != NULL) {
            if (min->word > curr2->word) 
                min = curr2;
            curr2 = curr2->next;
        }
        temp_word = curr1->word;
        curr1->word = min->word;
        min->word = temp_word;

        curr1 = curr1->next;
    }
}

void List :: deleteList() {
    Node *curr = head;

    if (curr == NULL)
        return;
    
    while(curr->next != NULL) {
        Node *temp = curr->next;
        delete(curr);
        curr = temp;
    }
    delete(curr);
}

string List :: print() {
    Node *curr = head;
    string output = "";
    while(curr != NULL) {
        output = output + " " + curr->word;
        curr = curr->next;
    }
    return output;
}

string List :: print(string delim) {
    if (head == NULL)
        return "";
    string output = head->word;
    Node *curr = head->next;
    while (curr != NULL) {
        output = output + delim + curr->word;
        curr = curr->next;
    }
    return output;
}
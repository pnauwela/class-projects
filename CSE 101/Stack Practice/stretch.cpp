#include <iostream>
#include <string>
#include <vector>
#include <stack>

#include "list.h"

using namespace std;

void generateStretch(const string& input, int k, string current, int index, vector<string>& result) {
    if (index == input.length()) {
        result.push_back(current);
        return;
    }

    for (int i = 1; i <= k; i++) {
        generateStretch(input, k, current + input[index], index + 1, result);
        current += input[index];
    }
}

vector<string> stretch(const string& input, int k) {
    vector<string> result;
    generateStretch(input, k, "", 0, result);
    return result;
}

List stretchStack(string input, int k) {
    List result;
    stack<pair<string, int>> stretching;
    stretching.push(make_pair("", 0));

    while (!stretching.empty()) {
        string current = stretching.top().first;
        int index = stretching.top().second;
        stretching.pop();

        if (index == input.length()) {
            result.insert(current);
        }
        else {
            for (int i = 1; i <= k; i++) {
                stretching.push(make_pair(current + input[index], index + 1));
                current += input[index];
            }
        }
    }
    return result;
}

int main() {
    string input = "abc";
    int k = 2;
    vector<string> stretched = stretch(input, k);

    List myList = stretchStack(input, k);
    cout << myList.print("\n") << endl;
    cout << "printed stack" << endl;
    for (const string& str : stretched) {
        cout << str << endl;
    }

    return 0;
}
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <format>
#include <algorithm>
#include <cstdlib>
#include <sstream>

using namespace std;

int find(int val, vector<int> arr) {
  for (int i = 0; i < arr.size(); i++) {
    if (arr[i] == val) 
      return i;
  }
  return -1;
}

int find(int val, int order, vector<pair<int, int>> pair) {
  if (order == 1) {
    for (int i = 0; i < pair.size(); i++) {
      if (pair[i].first == val) {
        return i;
      }
    }
  } else if (order == 2) {
    for (int j = 0; j < pair.size(); j++) {
      if (pair[j].second == val) {
        return j;
      }
    }
  }
  return -1;
}

int place(int c, int start, vector<pair<int, int>> pair, vector<int> pos, vector<int> neg, int n) {
  for (int r = start; r <= n; r++) {
    int find_pair = find(r, 2, pair);
    int find_pos = find(r + c, pos);
    int find_neg = find(r - c, neg);
    if ((find_pair >= 0) || (find_pos >= 0) || (find_neg >= 0)) {
      continue;
    } else {
      return r;
    }
  }
  return -1;
}


int main(int argc, char **argv) {
  if (argc < 3) {
    cout << "invalid arguments" << endl;
    return 0;
  }
  //do input handling stuff
  ifstream input;
  ofstream output;
  vector<int> in_str;
  string raw;
  vector<pair<int, int>> pair;
  vector<int> pos_diag;
  vector<int> neg_diag;
  int n;  //first at input
  int c = 1;
  int start = 1;

  input.open(argv[1]);
  output.open(argv[2]);

  while (getline(input, raw)) {
    istringstream iss(raw);
    iss >> n;

    int set_col, set_row;
    while(iss>>set_col && iss>>set_row) {
        pair.push_back(make_pair(set_col, set_row));
    }
    cout << n << endl;

    int set = pair.size(); //size of pair after input queens are placed
  
    do {
      int col = find(c, 1, pair);
      if (col > 0) {
        continue;
      } else {
        int row = place(c, start, pair, pos_diag, neg_diag, n);
        if (row < 0) {
          if (pair.size() - 1 < set) {
            output << "No solution" << endl;
            return 0;
          }
          start = pair.back().second;
          pair.pop_back();
          pos_diag.pop_back();
          neg_diag.pop_back();
          continue;
        } else {
          pair.push_back(make_pair(c, row));
          pos_diag.push_back(row + c);
          neg_diag.push_back(row - c);
          start = 1;
          c++;
        }
      }
    } while ((pair.size() < n) && (c <= n));

    //sort the vector  
    sort(pair.begin(), pair.end());
    //printing output
    for (int p = 0; p < pair.size(); p++) {
      int p_col = pair[p].first;
      int p_row = pair[p].second;
      output << p_col << " " << p_row;
    }
  }
  input.close();
  output.close();
  return 0;
}
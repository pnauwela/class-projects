#include <iostream>
#include <string>
#include <stack>

#include "list.h"

using namespace std;

void permute(string* arr, int& ind, string& wrd, int it) {
	if (it == wrd.length()) {
		arr[ind++] = wrd;
	} else {
		for (int i = it; i < wrd.length(); i++) {
			swap(wrd[i], wrd[it]);
			permute(arr, ind, wrd, it + 1);
			swap(wrd[i], wrd[it]);
		}
	}
}


List stackPermute(string input) {
    List myList;
    stack<pair<string, int>> stack;
    stack.push(make_pair(input, 0));

    while (!stack.empty()) {
        string curr = stack.top().first;
        int index = stack.top().second;
        stack.pop();

        if (index == curr.size()) {
            myList.insert(curr);
        } else {
            for (int i = index; i < curr.size(); i++) {
                swap(curr[index], curr[i]);
                stack.push(make_pair(curr, index + 1));
                swap(curr[index], curr[i]);
            }
        }
    }
    return myList;
}

int main(int argc, char **argv) {
	string a = "ABCD";
	string arr[100];
	int ind = 0;
	permute(arr, ind, a, 0);

    List StackPermute = stackPermute(a);
    StackPermute.sort();
    cout << StackPermute.print("\n") << endl;

    cout << "done printing the stack" << endl;
	for (int i = 0; i < ind; i++) {
		cout << arr[i] << endl;
	}
	return 0;
}
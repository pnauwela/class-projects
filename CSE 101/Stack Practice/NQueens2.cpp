#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

// Function to check if placing a queen at position (row, col) is valid
bool isSafe(vector<pair<int, int>>& queens, int col, int row) {
    for (const auto& queen : queens) {
        int qCol = queen.first;
        int qRow = queen.second;
        if (qRow == row || qCol == col || abs(qRow - row) == abs(qCol - col)) {
            return false; // Attack found
        }
    }
    return true; // No attack found
}

pair<int, int> findNextPosition(vector<pair<int, int>>& queens, int col, int startRow, int boardSize) {
    for (int row = startRow; row <= boardSize; row++) {
        if (isSafe(queens, col, row)) {
            return {col, row}; // Valid position found
        }
    }
    return {-1, -1}; // No valid position found
}

// Function to solve N-Queens problem iteratively with fixed placements
void solveNQueens(int boardSize, vector<pair<int, int>>& queens, ofstream& outFile) {
    stack<pair<int, int>> queenStack;
    for (const auto& queen : queens) {
        queenStack.push(queen);
    }

    while (!queenStack.empty()) {
        pair<int, int> currentQueen = queenStack.top();
        queenStack.pop();

        int currentCol = currentQueen.first;
        int currentRow = currentQueen.second;

        // Find next column to process
        int nextCol = currentCol + 1;
        if (nextCol > boardSize) {
            nextCol = 1; // Wrap around to the beginning if we reach the last column
        }

        // Skip the column if a queen is already placed there
        bool skipColumn = false;
        for (const auto& queen : queens) {
            if (queen.first == nextCol) {
                skipColumn = true;
                break;
            }
        }

        if (skipColumn) {
            continue; // Skip this column
        }

        // Try to find next valid position in the current column
        pair<int, int> nextPosition = findNextPosition(queens, nextCol, 1, boardSize);

        if (nextPosition.first != -1) {
            // Valid position found, push it onto the stack
            queens.push_back(nextPosition);
            queenStack.push(nextPosition);

            // If all queens are placed, print the positions and return
            if (queens.size() == boardSize) {
                for (const auto& queen : queens) {
                    outFile << queen.first << " " << queen.second << " ";
                }
                outFile << endl;
                return;
            }
        } else {
            // No valid position found, backtrack to the previous queen
            queens.pop_back();
        }
    }

    // If control reaches here, no solution is found
    outFile << "No solution" << endl;
}

int main(int argc, char **argv) {
    if (argc < 3) {
        cout << "not enough arguments" << endl;
        return 0;
    }
    ifstream input;
    ofstream output;

    string raw;
    input.open(argv[1]);
    output.open(argv[2]);

    while(getline(input, raw)) {
        vector<pair<int, int>> pairs;
        int n;

        istringstream iss(raw);
        iss >> n;
        int set_col, set_row;
        while (iss>>set_col && iss>>set_row){
            pairs.push_back({set_col, set_row});
        }
        solveNQueens(n, pairs, output);
    }
    input.close();
    output.close();
    return 0;
}
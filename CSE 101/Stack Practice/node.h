#ifndef NODE_H
#define NODE_H

#include <string>
using namespace std;

class Node {
    public:
        string word;
        Node *next;
        Node();
        Node(string);
};

#endif
#include <iostream>
#include <vector>
#include <string>
#include <stack>

#include "list.h"

using namespace std;

void generateBubblings(const string& input, int index, vector<string>& result) {
    if (index == input.size() - 1) {
        result.push_back(input);
        return;
    }

    generateBubblings(input, index + 1, result);

    string temp = input;
    swap(temp[index], temp[index + 1]);
    generateBubblings(temp, index + 1, result);
}

vector<string> bubbling(const string& input) {
    vector<string> result;
    generateBubblings(input, 0, result);
    return result;
}

List stackBubble(string input) {
    List myList;
    stack<pair<string, int>> bubbleStack;
    bubbleStack.push(make_pair(input, 0));

    while (!bubbleStack.empty()) {
        string str = bubbleStack.top().first;
        int index = bubbleStack.top().second;
        bubbleStack.pop();

        if (index == str.size() - 1) {
            myList.insert(str);
        } else {
            bubbleStack.push(make_pair(str, index + 1));

            string tmp = str;
            swap(tmp[index], tmp[index + 1]);
            bubbleStack.push(make_pair(tmp, index + 1));
        }
    }
    return myList;
}

int main() {
    string input = "abc";
    vector<string> bubblings = bubbling(input);

    List result = stackBubble(input);
    cout << result.print("\n") << endl;

    cout << "stack done" << endl;

    for (const auto& bubbling : bubblings) {
        cout << bubbling << endl;
    }

    return 0;
}
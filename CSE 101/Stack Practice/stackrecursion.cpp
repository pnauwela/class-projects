#include "list.h"
#include <iostream>
#include <stack>
#include <stdexcept>
#include <fstream>
#include <array>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

class Pair {
    public:
        string in_str;
        string fixed_str;
};

List allSubSeqStack(string input_str) {
    List final_output;                  //linked list containing each of the substrings
    stack<class Pair> recurStack;       //stack with a pair of strings
    Pair init, stacktop;                //pairs to be used later
    init.in_str = input_str;            //assigning first pair
    init.fixed_str = "";
    recurStack.push(init);              //push initial pair

    while(!recurStack.empty()) {
        stacktop = recurStack.top();        //get top item
        recurStack.pop();                   //take it off
        if (stacktop.in_str.length() == 0) {
            final_output.insert(stacktop.fixed_str);    //if length is 0 it's the end
            continue;
        }
        char last = stacktop.in_str.back();                 //get last character from in_str
        stacktop.in_str.pop_back();                         //delete it
        Pair to_push;                                       //new pair of items to be pushed
        to_push.in_str = stacktop.in_str;                   //copy current stacktop
        to_push.fixed_str = stacktop.fixed_str;
        recurStack.push(to_push);                           //push item back on stack
        to_push.fixed_str = last + stacktop.fixed_str;      //put last character from in_str into fixed
        recurStack.push(to_push);                           //push the character added to the stack
    }
    return final_output;
}

//recursive method
void allSubSeqRec(string in_str, string fixed_str, List *list_ptr)
{
    if (in_str.length() == 0){
        list_ptr->insert(fixed_str);
        return;
    }
    char top = in_str.back();
    in_str.pop_back();
    allSubSeqRec(in_str, fixed_str, list_ptr);
    allSubSeqRec(in_str, top + fixed_str, list_ptr);
    return;
}

List allSubSeq(string in_str){
    List *final_output = new List();
    allSubSeqRec(in_str, "", final_output);
    return *final_output;
}

int main(int argc, char **argv) {
    if (argc < 3) 
        throw std::invalid_argument("Usage: ./hello <INPUT FILE> <OUTPUT FILE>"); // throw error
    ifstream input;
    ofstream output;

    input.open(argv[1]);
    output.open(argv[2]);

    string in_str;

    getline(input, in_str);
    
    List myList = allSubSeqStack(in_str);
    myList.sort();

    output << myList.print("\n") << endl;

    myList.deleteList();
    input.close();
    output.close();
}
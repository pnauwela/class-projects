#ifndef LIST_H 
#define LIST_H 

#include "node.h"

class List {
    public:
        Node *head;
        List();
        void deleteList();
        Node* find(string);
        void insert(string);
        void sort();
        string print();
        string print(string delim);
};

#endif
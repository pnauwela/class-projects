#include <iostream>
#include <string>
#include <vector>
#include <stack>

#include "list.h"

using namespace std;

void generateLanguage(string input, int k, vector<string>& result, string current = "") {
    if (current.size() <= k) {
        result.push_back(current);
    }

    if (current.size() < k) {
        for (char c : input) {
            generateLanguage(input, k, result, current + c);
        }
    }
}

List language(string input, int k) {
    List myList;
    stack<pair<string, int>> stk;
    stk.push(make_pair("", 0));

    while(!stk.empty()) {
        string curr = stk.top().first;
        int len = stk.top().second;
        stk.pop();

        myList.insert(curr);

        if (len < k) {
            for (char c : input) {
                string next = curr + c;
                stk.push(make_pair(next, len + 1));
            }
        }
    }
    return myList;
}

int main() {
    string input = "abc";
    int k = 2;

    vector<string> result;
    generateLanguage(input, k, result);

    List output = language(input, k);
    output.sort();
    cout << output.print("\n") << endl;
    cout << "\nend of recursive" << endl;
    for (const auto& str : result) {
        cout << str << endl;
    }

    return 0;
}
#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

struct Node {
    string actor;
    vector<pair<Node*, string>> connections;
    int index;
    bool visited;
};


class Graph {
    private:
        vector<Node*> actorList;    //nodes get added to this list when they get created inside createGraph()
    public:
        Node *insert(string);
        Node *find(string);
        void makeConnection(Node*, Node*, string);
        void createGraph();
        string BFS(string, string);
        void deleteGraph();    
};

#endif 
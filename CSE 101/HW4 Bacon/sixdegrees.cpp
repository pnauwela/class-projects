#include "graph.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

int main(int argc, char **argv) {
    if (argc < 3) {
        cout << "Usage ./sixdegrees <INPUT FILE> <OUTPUT FILE>" << endl;
        return 1;
    }

    ifstream input;
    ofstream output;

    input.open(argv[1]);
    output.open(argv[2]);

    Graph myGraph;

    myGraph.createGraph();

    string raw;

    while(getline(input, raw)) {
        istringstream iss(raw);
        string act1, act2;

        iss >> act1 >> act2;

        output << myGraph.BFS(act1, act2) << endl;
    }

    myGraph.deleteGraph();

    return 0;
}
#include "graph.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <queue>
#include <cstdlib>
#include <fstream>

using namespace std;

Node *Graph :: insert(string name) {
    Node* to_add = new Node;
    to_add->visited = false;
    to_add->actor = name;
    to_add->index = 0;
    actorList.push_back(to_add);
    return to_add;
}

Node *Graph :: find(string name) {
    for (int i = 0; i < actorList.size(); i++) {
        if (actorList[i]->actor == name) {
            return actorList[i];
        }
    }
    return NULL;
}

void Graph :: makeConnection(Node *actor1, Node *actor2, string movie) {
    actor1->connections.push_back(make_pair(actor2, movie));
}

void Graph :: createGraph() {
    ifstream list;
    list.open("cleaned_movielist.txt");

    string raw, temp_actor, movie;
    vector<Node*> connection_list;
    while(getline(list, raw)) {
        istringstream iss(raw);

        iss >> movie;
        
        while(iss>>temp_actor) {
            if (actorList.size() == 0) {
                Node *first = insert(temp_actor);
                connection_list.push_back(first);
            } else {
                Node *temp = find(temp_actor);
                if (temp) {
                    connection_list.push_back(temp);
                } else {
                    temp = insert(temp_actor);
                    temp->index = actorList.size();
                    connection_list.push_back(temp);
                }
            }
            
        }
        for (int j = 0; j < connection_list.size(); j++) {
            for (int k = 0; k < connection_list.size() && j!=k; k++) {
                makeConnection(connection_list[j], connection_list[k], movie);
            }
        }
        connection_list.clear();
    }
}

string Graph :: BFS(string act1, string act2) {
    if (act1 == act2) {
        return act1;
    } 

    Node *start = find(act1);
    Node *endpoint = find(act2);

    if (start == NULL || endpoint == NULL) {
        return "Not present";
    }

    cout << "Searching" << endl;

    vector<pair<Node*, string>> pred;
    pred.reserve(actorList.size());

    queue<Node*> q;
    q.push(start);
    bool end = false;
    
    while(!q.empty()) {
        Node *u = q.front();
        q.pop();

        for (int j = 0; j < u->connections.size(); j++) {
            if (!u->connections[j].first->visited) {
                Node *temp = u->connections[j].first;
                pred[temp->index] = make_pair(temp, u->connections[j].second);
                if (temp->actor == act2) {
                    end = true;
                    break;
                }
                q.push(temp);
                temp->visited = true;
            }
        }
        if (end) {
            break;
        }
    }
    vector<pair<Node*, string>> print_list;
    while(pred[endpoint->index].first != start) {
        print_list.push_back(pred[endpoint->index]);
        endpoint = pred[endpoint->index].first;
    }
    string to_return = start->actor + " -(";
    Node *curr_print;
    string movie_connection;
    while(print_list.size() > 1) {
        curr_print = print_list.back().first; 
        movie_connection = print_list.back().second;
        print_list.pop_back();
        to_return += movie_connection + ")- " + curr_print->actor + " -(";
    }
    to_return += print_list.back().second + ")- " + act2;

    return to_return;
}

void Graph :: deleteGraph() {
    for (int i = 0; i < actorList.size(); i++) {
        delete(actorList[i]);
    }
}
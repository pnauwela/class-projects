import os
import pickle

def driver_scheduling():

    scheduledDates = []
    scheduledTimes = []
    
    newScheduleDates = ''
    newScheduleTimes = ''
    while newScheduleDates != 'quit':
        newScheduleDates = input("What days are you working, or type quit when you're done\n")

        if newScheduleDates != 'quit':
            scheduledDates.append(newScheduleDates)
            
    while newScheduleTimes != 5.00:
        newScheduleTimes = float(input("What times of day, everyone is required to work until 5.00 so when all times are inputed, type 5.00\n"))
        
        if newScheduleDates != 5.00:
            scheduledTimes.append(newScheduleTimes)
    
    file_object = open('taxiSchedule', 'a')

    file_object.writelines(str(scheduledDates))
    file_object.write("\n")
    
    file_object.writelines(str(scheduledTimes))
    file_object.write("\n")

    file_object.close()

driver_scheduling()

def driver_search():
    
    file_object = open ("taxiSchedule", "r")
    is_file_empty = os.path.getsize("taxiSchedule") == 0
    if not is_file_empty:
        search_times = input("Enter time you would like to schedule\n")
        is_contact_found = False
        list_driver = pickle.load(file_object)
        for each_time in list_driver:
            driver_time = each_time.name
            searc_time = search_times.lower()
            driver_time = driver_time.lower()
            if(driver_time == search_times):
                print(each_time)
                is_contact_found = True
                break
        if not is_contact_found:
            print("No free drivers at that time, please try again with a different time")
    else:
        print("There is no times scheduled at all")
    file_object.close()

driver_search()

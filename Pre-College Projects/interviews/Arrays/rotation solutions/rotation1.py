def rotleft(a, d):
    b = []
    for i in range(-d, len(a)):
        b.append(a[i])
    b.extend(a[0:d])
    return b 

rotleft([1,2,3,4,5,6,7],2)
rotleft([12,34,20,11,10,56],1)


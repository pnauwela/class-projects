#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <tchar.h>


DWORD getProcessIdByName(const char* exename) {
  HANDLE hSnapshot = CreateToolhelp32Snapshot (TH32CS_SNAPPROCESS, 0) ;
  PROCESSENTRY32 pe32 ;

  if (Process32First (hSnapshot, &pe32)) {
    do {
      if (strcmp(exename, pe32.szExeFile) == 0) {
	//printf ("found process:%ld for exe name:%s\n", pe32.th32ProcessID, pe32.szExeFile);
	return pe32.th32ProcessID;
      }
    } while (Process32Next (hSnapshot, &pe32));
  }
  CloseHandle (hSnapshot);

  return 0;
}

void killProcessByName(const char* exename) {
  DWORD dwProcessId = getProcessIdByName(exename);
  HANDLE  hProcess;
  BOOL    fResult = FALSE;
  
  if (dwProcessId == 0) {
    printf("failed to get process id for %s\n", exename);
  }

  hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
  if(hProcess != NULL){
    //printf("going to terminate process: %ld\n", dwProcessId);
    fResult = TerminateProcess(hProcess, 0);
    WaitForSingleObject(hProcess, 500);
    CloseHandle(hProcess);
  }
}


int _tmain(int argc, _TCHAR* argv[]) {
  killProcessByName("chrome.exe");
  return 0;
}
import requests

class Currency_convertor:
    #empty dict to store conversion rates
    rates = {}
    def __init__(self, url):
        data = requests.get(url).json()
        
        #extracting rates from json
        
        self.rates = data["rates"]
    
    #cross multiplication for ammount and conversion rates
    
    def convert(self, from_currency, to_currency, amount):
        initial_amount = amount
        if from_currency != 'EUR' :
            amount = amount / self.rates[from_currency]
        
        #limiting the precision to 2 decimal places
        amount = round(amount * self.rates[to_currency], 2)
        print('{} {} = {} {}'. format(initial_amount, from_currency, amount, to_currency))
    
#driver code
if __name__ == "__main__":
    
    YOUR_ACCES_KEY = 'GET YOUR ACCESS KEY FROM fixer.io'
    url = str.__add__('http://data.fixer.io/api/latest?access_key=a6708da962b946576007cc2da356edae&format=1', YOUR_ACCES_KEY)   #my access key
    c = Currency_convertor(url)
    from_country = input("From Country: ")
    to_country = input("TO Country: ")
    amount = int(input("Amount: "))
    
    c.convert(from_country, to_country, amount)
import time
import urllib
import threading
from queue import Queue 
from random import choice

def get_random_article(namespace=None):
    try:
        url = 'http://en.wikipedia.org/wiki/Special:Random'
        if namespace != None:
            url += '/' + namespace
        req = urllib.Request(url, None, { 'User-Agent'  : 'x'})
        page = urllib.urlopen(req).readlines()
        return page
    except (urllib.HTTPError, urllib.URLError):
        print("Failed to get article")
        raise

class DocumentDownloader(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self._stop = threading.Event()
        self.articles = []
        self.queue = queue
    
    def stop(self):
        self._stop.set()
    
    def stopped(self):
        return self._stop.isSet()
    
    def get_articles(self):
        return self.articles
    
    def run(self):
        while True:
            if self.stopped():
                return
            if self.queue.empty():
                time.sleep(0.1)
                continue
            try:
                namespace = self.queue.get()
                article = get_random_article()
                self.articles.append(article)
                print("Successfully processed namespace: ", namespace)
                print("by thread: ", self.ident)
            except:
                print("Failed to process namespace: ", namespace)
                
def get_random_documents(num_documents, num_threads = 4):
    wiki_namespace = """
    Main
    User
    Wikipedia
    File
    MediaWiki
    Template
    Help
    Category
    Portal
    Book""".split
    
    q = Queue()
    threads = []
    
    try:
        for i in range(num_threads):
            thread = DocumentDownloader(q)
            thread.setDaemon(True)
            thread.start()
            threads.append(thread)
        
        for i in range(num_documents):
            namespace = choice(wiki_namespace)
            q.put(namespace)
        
        while not q.empty():
            time.sleep(1.0)
        
        for thread in threads:
            thread.stop()
        for thread in threads:
            thread.join()
    
    except:
        print("Main thread hit exception")
        for thread in threads:
            thread.stop()
        for thread in threads:
            thread.join
        raise
    
    documents = []
    for thread in threads:
        documents.extend(thread.get_articles())
        
    return documents

if __name__ == "__main__":
    documents = get_random_documents(10)
    for document in documents:
        print(document[:10])
            
        
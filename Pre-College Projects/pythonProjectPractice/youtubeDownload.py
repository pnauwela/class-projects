from pytube import YouTube

link = input("Enter the link: ")
yt = YouTube(link)

print("Title:", yt.title)

print("Numbe of views:", yt.views)

print("Length of video:", yt.length, "seconds")

print("Description:", yt.description)

print("Ratings:", yt.rating)

#filter out only audio streams of it
print(yt.streams.filter(only_audio = True))

#video only streams
print(yt.streams.filter(only_video = True))

print(yt.streams.filter(progressive = True))

ys = yt.streams.get_highest_resolution()

ys.download('D:/chrome downloads/')
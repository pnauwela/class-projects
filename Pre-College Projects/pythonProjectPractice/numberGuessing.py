import random
import math

lower = int(input('enter lower bound: '))
upper = int(input('enter upper bound: '))

x = random.randint(lower, upper)
print("\n\tYou've only ", round(math.log(upper - lower + 1, 2)),
      " chances to guess the interger\n")

count = 0

while count < math.log(upper - lower + 1, 2):
    count += 1
    
    guess = int(input('guess a number: '))
    
    if x == guess:
        print("You did it in ", count, "guesses")
        break
    elif x > guess:
        print("you guessed too low")
    elif x < guess:
        print("you guessed too high")
        
if count >= math.log(upper - lower + 1, 2):
    print("\the number is %d"%x)
    print("\tBetter luck next time")
    
import random

name = input("what is your name ")

print("Good Luck ", name)

words = ['rainbow', 'computer', 'science', 'programing', 'python', 'mathematics', 
         'player', 'condition', 'reverse', 'water', 'bored', 'geeks']

word = random.choice(words)

print("Guess the characters")

guesses = ''

turns = 12

while turns > 0:
    failed = 0
    
    for char in word:
        if char in guesses:
            print(char)
        else:
            print("_")
            failed += 1
    
    if failed == 0:
        print("You win")
        print("The word is", word)
        break
    
    guess = input("guess a character:")
    
    guesses += guess
    
    if guess not in word:
        
        turns -= 1
        
        print("Wrong")
        
        if turns == 0:
            print("You lose")
            print("the word was", word)
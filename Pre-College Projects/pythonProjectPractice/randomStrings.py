import random
import string

def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)

get_random_string(8)
get_random_string(8)
get_random_string(6)

def get_random_password(leg):
    mixLetters = string.ascii_letters
    resultStr = ''.join(random.choice(mixLetters) for i in range(leg))
    print("Random string is:", resultStr)
    
get_random_password(8)
get_random_password(8)

def random_string(stringLength):
    sample_letters = input("Type random letters: ")
    stringResult = ''.join((random.choice(sample_letters) for i in range(stringLength)))
    print("Random string is:", stringResult)
    
random_string(5)
random_string(5)

def get_random_numbers_and_string(pswrdLength):
    letters_and_digits = string.ascii_letters + string.digits
    results = ''.join((random.choice(letters_and_digits) for i in range(pswrdLength)))
    print("Random mix of strings and numbers:", results)

get_random_numbers_and_string(8)
get_random_numbers_and_string(20)

def get_random_password_string(lengthywengthy):
    password_characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(password_characters) for i in range(lengthywengthy))
    print("Random string password is:", password)

get_random_password_string(10)
get_random_password_string(10)

def get_random_password():
    random_source = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_lowercase)
    password += random.choice(string.ascii_uppercase)
    password += random.choice(string.digits)
    password += random.choice(string.punctuation)

    for i in range(6):
        password += random.choice(random_source)

    password_list = list(password)
    random.SystemRandom().shuffle(password_list)
    password = ''.join(password_list)
    return password

print("First Random Password is ", get_random_password())
print("Second Random Password is ", get_random_password())


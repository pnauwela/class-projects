from django.urls import path
from web_project.hello import views

urlpatterns = [
    path("", views.home, name="home"),
]
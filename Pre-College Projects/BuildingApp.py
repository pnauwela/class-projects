import kivy
import random

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout

"""
class MainApp(App):
    def build(self):
        label = Label(text='this do be kinda useless so far',
                      size_hint = (.5, .5),
                      pos_hint = {'center_x': .5, 'center_y': .5})
        return label
        img = Image(source = 'G:\Kristofer-Yee.jpg',
                size_hint = (1, .5),
                pos_hint = {'center_x':.5, 'center_y':.5})
        return img
    
if __name__ == '__main__':
    app = MainApp()
    app.run()
"""

red = [1, 0, 0, 1]
green = [0, 1, 0, 1]
blue = [0, 0, 1, 1]
purple = [1, 0, 1, 1]
"""
class HBoxLayoutExample(App):
    def build(self):
        layout = BoxLayout(padding = 10)
        colors = [red, green, blue, purple]
        
        for i in range(5):
            btn = Button(text = "Button #%s" % (i + 1),
                         background_color = random.choice(colors)
                         )
            layout.add_widget(btn)
        return layout

if __name__ == "__main__":
    app = HBoxLayoutExample()
    app.run() """
    

class MainApp(App):
    def build(self):

        colors = [red, green, blue, purple]
        
        button = Button(text = 'Hello from Kivy',
                        background_color = random.choice(colors),
                        size_hint = (.5, .5),
                        pos_hint = {'center_x': .5, ' center_y': .5})
        button.bind(on_press=self.on_press_button)
        
        if button == True:
            
            img = Image(source = 'G:\Kristofer-Yee.jpg',
                        size_hint = (1, .5),
                        pos_hint = {'center_x':.5, 'center_y':.5})
            return img
        
        return button
    
    def on_press_button(self, instance):
        print('Boop')
        """img = Image(source = 'G:\Kristofer-Yee.jpg',
                size_hint = (1, .5),
                pos_hint = {'center_x':.5, 'center_y':.5})
        return img"""
    
if __name__ == '__main__':
    app = MainApp()
    app.run()
/*#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

//line plot 0.0-1.0
float plot(vec2 st) {
    return smoothstep(0.02, 0.0, abs(st.y - st.x));
}

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution;

    float y = st.x;

    vec3 color = vec3(y);

    //line plot
    float pct = plot(st);
    color = (1.0-pct)*color+pct*vec3(0.0, 0.0, 1.0);

    gl_FragColor = vec4(color, 1.0);
}*/
//1-1 x-y ratio for gradiant of color

//exponential ratio
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

/*float plot(vec2 st, float pct) {
    return smoothstep( pct-0.02, pct, st.y) - 
           smoothstep( pct, pct+0.02, st.y);
}

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution;

    float y = smoothstep(0.2,0.5,st.x) - smoothstep(0.5,0.8,st.x);

    vec3 color = vec3(y);

    float pct = plot(st, y);
    color = (1.0-pct)*color+pct*vec3(0.0, 0.0, 1.0);

    gl_FragColor = vec4(color, 1.0);
}
accessing colors
vec4 vector;

vector[0] = vector.r = vector.x = vector.s;
vector[1] = vector.g = vector.y = vector.t;
vector[2] = vector.b = vector.z = vector.p;
vector[3] = vector.a = vector.w = vector.q;
*/
 
void main() {
    vec3 yellow, magenta, green;

    //yellow
    yellow.rg = vec2(1.0);  //1 to red and green
    yellow[2] = 0.0;

    //gl_FragColor = vec4(yellow, 1.0);     running it

    //magenta
    magenta = yellow.rbg;   //assign channels with green and blue swapped

    //gl_FragColor = vec4(magenta, 1.0);

    //green
    green.rgb = yellow.bgb;

    //gl_FragColor = vec4(green, 1.0);
} 


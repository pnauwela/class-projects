#ifdef GL_ES
precision mediump float;
#endif 

#define PI 3.1415926535897
#define HALF_PI 1.5707963267948966

uniform vec2 u_resolution;
uniform float u_time;

float bounceOut(float t) {
  const float a = 4.0 / 11.0;
  const float b = 8.0 / 11.0;
  const float c = 9.0 / 10.0;

  const float ca = 4356.0 / 361.0;
  const float cb = 35442.0 / 1805.0;
  const float cc = 16061.0 / 1805.0;

  float t2 = t * t;

  return t < a
    ? 7.5625 * t2
    : t < b
      ? 9.075 * t2 - 9.9 * t + 3.4
      : t < c
        ? ca * t2 - cb * t + cc
        : 10.8 * t * t - 20.52 * t + 10.72;
}

float backIn(float t) {
    return pow(t, 3.0) - t * sin(t * PI);
}

float sineIn(float t) {
    return sin((t - 1.0) * HALF_PI) + 1.0;
}

void main() {
    vec3 colorA = vec3(0.7804, 0.4275, 0.8667);
    vec3 colorB = vec3(0.4275, 0.6314, 0.902);

    float t = u_time*0.3;
    float pct = (backIn(sineIn(fract(t))));
    
    gl_FragColor = vec4(vec3(mix(colorA, colorB, pct)), 1.0);
}

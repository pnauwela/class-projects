#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897
#define HALF_PI 1.5707963267948966

uniform vec2 u_resolution;
uniform float u_time;

float bounceOut(float t) {
  const float a = 4.0 / 11.0;
  const float b = 8.0 / 11.0;
  const float c = 9.0 / 10.0;

  const float ca = 4356.0 / 361.0;
  const float cb = 35442.0 / 1805.0;
  const float cc = 16061.0 / 1805.0;

  float t2 = t * t;

  return t < a
    ? 7.5625 * t2
    : t < b
      ? 9.075 * t2 - 9.9 * t + 3.4
      : t < c
        ? ca * t2 - cb * t + cc
        : 10.8 * t * t - 20.52 * t + 10.72;
}

float quarticOut(float t) {
    return pow(t - 1.0, 3.0) * (1.0 - t) + 1.0;
}

float bounceInOut(float t) {
    return t < 0.5
        ? 0.5 * (1.0 - bounceOut(1.0 - t * 2.0))
        : 0.5 * bounceOut(t * 2.0 - 1.0) + 0.5;
}

float sineInOut(float t) {
    return -0.5 * (cos(PI * t) - 1.0);
}

float elasticInOut(float t) {
    return t < 0.5
        ? 0.5 * sin(+13.0 * HALF_PI * 2.0 * t) * pow(2.0, 10.0 * (2.0 * t - 1.0))
        : 0.5 * sin(-13.0 * HALF_PI * ((2.0 * t - 1.0) + 1.0)) * pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
}

void main() {
    vec3 colorA = vec3(0.9451, 0.4431, 0.8627);
    vec3 colorB = vec3(0.4824, 0.6941, 0.9333);

    float t = u_time*0.3;
    float pct = abs(quarticOut(bounceInOut(sineInOut(elasticInOut(fract(t))))));

    gl_FragColor = vec4(vec3(mix(colorA, colorB, pct)), 1.0);
}
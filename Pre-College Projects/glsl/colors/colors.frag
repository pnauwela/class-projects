#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

/* accessing colors
vec4 vector;
vector[0] = vector.r = vector.x = vector.s;
vector[1] = vector.g = vector.y = vector.t;
vector[2] = vector.b = vector.z = vector.p;
vector[3] = vector.a = vector.w = vector.q;

 
void main() {
    vec3 yellow, magenta, green;

    //yellow
    yellow.rg = vec2(1.0);  //1 to red and green
    yellow[2] = 0.0;

    //gl_FragColor = vec4(yellow, 1.0);     running it

    //magenta
    magenta = yellow.rbg;   //assign channels with green and blue swapped

    //gl_FragColor = vec4(magenta, 1.0);

    //green
    green.rgb = yellow.bgb;

    //gl_FragColor = vec4(green, 1.0);
} */

vec3 colorA = vec3(0.149, 0.141, 0.912);
vec3 colorB = vec3(1.000, 0.833, 0.224);

void main() {
    vec3 color = vec3(0.0);
    
    float pct = abs(sin(u_time));

    //mix uses pct to mix the colors 
    color = mix(colorA, colorB, pct);

    gl_FragColor = vec4(color, 1.0);
}

#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897
#define HALF_PI 1.5707963267948966

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

vec3 colorA = vec3(0.3451, 0.4784, 0.9216);
vec3 colorB = vec3(0.9529, 0.6275, 0.2);


float plot(vec2 st, float pct) {
    return  smoothstep(pct-0.01, pct, st.y) - 
            smoothstep(pct, pct+0.01, st.y);
}

float easeInOutCirc(float t) {
return t < 0.5
  ? (1.0 - sqrt(1.0 - pow(2.0 * t, 2.0))) / 2.0
  : (sqrt(1.0 - pow(-2.0 * t + 2.0, 2.0)) + 1.0) / 2.0;
}

float sineOut(float t) {
    return sin(t * HALF_PI);
}

float inverseSine(float t) {
    return sin(t / HALF_PI);
}

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    float t = u_time*0.01;

    float pct = abs(inverseSine((u_time)));

    //pct.r = smoothstep(0.0, 1.0, st.x);
    //pct.g = easeInOutCirc(sin(st.x*PI));
    //pct.b = pow(0.5, st.y);

    color = mix(colorA, colorB, pct); 

    //plotting transition lines
    //color = mix(color, vec3(1.0, 0.0, 0.0), plot(st, pct.r));
    //color = mix(color, vec3(0.0, 1.0, 0.0), plot(st, pct.g));
    //color = mix(color, vec3(0.0, 0.0, 1.0), plot(st, pct.b));

    gl_FragColor = vec4(color, 1.0);
}
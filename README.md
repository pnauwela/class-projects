# Extron ControlScript Programming
I have a large project that is the focus of my employment at UC Santa Cruz. This project includes some sensitive information
which I am not able to disclose publicly in a repository. If you would like to see this project as proof of experience 
with AV Tech, Python, or Extron specifically, please contact me at pnauwela@ucsc.edu. I will schedule a time to walk you 
through the project. The project is the largest display of my skill as a programmer, so it is unfortunate that I have to 
keep it private, but a showing and discussion of the project can be arranged.

# Class Projects

Collection point for all of the projects worked on during my time taking classes in university. 

# Folders:
Each class has a folder named after it. Within that folder are the projects for that class
